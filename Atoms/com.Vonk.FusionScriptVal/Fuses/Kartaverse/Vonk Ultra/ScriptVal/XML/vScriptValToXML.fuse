-- ============================================================================
-- modules
-- ============================================================================
-- This fuse requires the installation of xml2lua:
-- https://github.com/manoelcampos/xml2lua
-- The needed Lua module files are "xml2lua.lua", "XmlParser.lua", and "xmlhandler/tree.lua".
local xmlutils = self and require("xml2lua") or nil
local xmlhandler = self and require("xmlhandler/tree") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToXML"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\XML",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Casts a ScriptVal object into XML text.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutXML = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local sort = InSort:GetValue(req).Value

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    local xml_str = xmlutils.toXml(tbl)
    local out = Text(xml_str)

    OutXML:Set(req, out)
end
