-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointClamp"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Operators",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Clamp a Fusion Point object to specific boundaries",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Tracker",
})

function math.clamp(val, lower, upper)
    assert(val and lower and upper, "not very useful error message here")
    if lower > upper then lower, upper = upper, lower end -- swap if boundaries supplied the wrong way
    return math.max(lower, math.min(upper, val))
end

function Create()
    -- [[ Creates the user interface. ]]
    InPoint = self:AddInput("Point", "Point", {
        LINKID_DataType = "Point",
        LINK_Main = 1,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
    })

    InMinimum = self:AddInput("Minimum", "Minimum" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 2,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InMaximum = self:AddInput("Maximum", "Maximum" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 3,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 1.0
    })
    
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InPoint:SetAttrs({LINK_Visible = visible})
        InMinimum:SetAttrs({LINK_Visible = visible})
        InMaximum:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local point = InPoint:GetValue(req)
    
    local minimum = InMinimum:GetValue(req).Value
    local maximum = InMaximum:GetValue(req).Value

    local x = tonumber(point.X)
    local y = tonumber(point.Y)

    local outX = math.clamp(x, minimum, maximum)
    local outY = math.clamp(y, minimum, maximum)

    local out = Point(outX, outY)
    OutPoint:Set(req, out)
end
