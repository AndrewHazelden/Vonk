-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromSubtitle"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Subtitle",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion Text object by extracting text from SRT Subtitles.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
    self:BeginControlNest("Subtitles", "Subtitles", true, {})
        InData = self:AddInput("Input", "Input", {
            LINKID_DataType = "Text",
            INPID_InputControl = "TextEditControl",
            IC_NoLabel = true,
            TEC_Lines = 10,
            LINK_Main = 1
        })
        InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_Integer = true,
            INP_MaxScale = 100,
            INP_MinAllowed = 1,
            INP_Default = 20,
            LINK_Visible = true,
            INP_Passive = true,
            INP_DoNotifyChanged  = true,
        })
        InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
            LINKID_DataType = "Number",
            INPID_InputControl = "CheckboxControl",
            INP_Integer = true,
            INP_Default = 0.0,
            INP_External = false,
            INP_Passive = true,
            IC_Visible = false,
            INP_DoNotifyChanged = true
        })
        InRemoveHTML = self:AddInput("Remove HTML Tags", "RemoveHTML", {
            LINKID_DataType = "Number",
            INPID_InputControl = "CheckboxControl",
            INP_Integer = true,
            INP_Default = 1.0,
            INP_External = false,
            INP_DoNotifyChanged = true
        })
        InRemoveHour = self:AddInput("Remove +1 Hour Starting Timecode", "RemoveHour", {
            LINKID_DataType = "Number",
            INPID_InputControl = "CheckboxControl",
            INP_Integer = true,
            INP_Default = 1.0,
            INP_External = false,
            INP_DoNotifyChanged = true
        })
    self:EndControlNest()
    self:BeginControlNest("Timing", "Timing", true, {})
        InTimingMode = self:AddInput("Timing Mode", "TimingMode", {
            LINKID_DataType = "Number",
            INPID_InputControl = "MultiButtonControl",
            -- MBTNC_ForceButtons = true,
            --MBTNC_StretchToFit = true,
            INP_Default = 0,
            {MBTNC_AddButton = "Timecode"},
            {MBTNC_AddButton = "Index"},
            INP_DoNotifyChanged = true,
        })
        InFps = self:AddInput("FPS", "FPS", {
            LINKID_DataType = "Number",
            INPID_InputControl = "MultiButtonControl",
            -- MBTNC_ForceButtons = true,
            --MBTNC_StretchToFit = true,
            INP_Default = 2,
            {MBTNC_AddButton = "24"},
            {MBTNC_AddButton = "25"},
            {MBTNC_AddButton = "30"},
            {MBTNC_AddButton = "48"},
            {MBTNC_AddButton = "50"},
            {MBTNC_AddButton = "60"},
            {MBTNC_AddButton = "23.976"},
            {MBTNC_AddButton = "29.97"},
            {MBTNC_AddButton = "59.94"},
            INP_DoNotifyChanged = true,
        })
        -- Should the image sequence frame numbering be shifted (forwards/backwards) by the sequence offset value?
        InSequenceStartFrame = self:AddInput('Sequence Offset', 'SequenceStartFrame', {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            -- INPID_InputControl = "SliderControl",
            -- TEC_Lines = 1,
            INP_Integer = true,
            INP_MinScale = -100,
            INP_MaxScale = 100,
            INP_Default = 0,
            IC_Steps = 201,
            INP_MinAllowed = -1e+38,
            INP_MaxAllowed = 1e+38,
            -- INP_MinAllowed = -1000000,
            -- INP_MaxAllowed = 1000000,
            LINK_Main = 2,
        })
        InIndex = self:AddInput("Index", "Index", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_MinScale = 1,
            INP_MaxScale = 144,
            INP_Default = 1,
            INP_MinAllowed = 1,
            INP_MaxAllowed = 1e+38,
            INP_Integer = true,
            IC_Visible = false,
            LINK_Main = 3
        })
    self:EndControlNest()
    InShowInput = self:AddInput("Show Timing Inputs", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        --INPID_InputControl = "TextEditControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InIndex:SetAttrs({LINK_Visible = visible})
        -- InData:SetAttrs({LINK_Visible = visible})
        InSequenceStartFrame:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InData:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InData:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InTimingMode then
        if param.Value == 0.0 then
            -- Timecode
            InIndex:SetAttrs({IC_Visible = false})
            InFps:SetAttrs({IC_Visible = true})
            InSequenceStartFrame:SetAttrs({IC_Visible = true})
        else
            -- Index
            InIndex:SetAttrs({IC_Visible = true})
            InFps:SetAttrs({IC_Visible = false})
            InSequenceStartFrame:SetAttrs({IC_Visible = false})
        end
    end
end

function ReadSRT(data, index)
    --[[
        Extracts a single line of text from an SRT Subtitle

        :param data: A multi-line block of text
        :type data: string

        :param index: Line number to extract
        :type index: integer

        :rtype: string
    ]]--

    local currentLine = 0
    for i in string.gmatch(data, "(%d+[:]%d+[:]%d+[,]%d+.-%d+[:]%d+[:]%d+[,]%d+[\n].-[\n][\n]+%d+)") do
        currentLine = currentLine + 1;
        --print(currentLine, ":", i)

        if currentLine == index then
            return i
        end
    end

    return ""
end

function SplitTimecode(data, fps, removeHour)
    if data then 
        local hours, minutes, seconds, ms = string.match(data, "(%d+)[:](%d+)[:](%d+)[,](%d+)")
        if removeHour == 1 then
            hours = hours - 1
        end

        local frame = math.floor(fps * ((ms * 0.001) + seconds + (60 * minutes) + (3600 * hours)))
        return frame
    end

    return 0
end

function SearchByIndex(data, index, fps, removeHour)
    local str = ReadSRT(data, index)
    --print(index, str)

    if data == "" or str == "" then
        return 0, 0, ""
    end

    local timecode_start_str, timecode_end_str, subtitle_str = string.match(str, "(%d+[:]%d+[:]%d+[,]%d+).-(%d+[:]%d+[:]%d+[,]%d+)[\n](.-)[\n][\n]+%d+")

    local timecode_start_num = SplitTimecode(timecode_start_str, fps, removeHour)
    local timecode_end_num = SplitTimecode(timecode_end_str, fps, removeHour)
    --print(timecode_start_str, timecode_start_num, timecode_end_str, timecode_end_num, subtitle_str)

    return timecode_start_num, timecode_end_num, subtitle_str
end

function SearchByTimecode(data, time, fps, removeHour)
    local currentLine = 0
    for i in string.gmatch(data, "(%d+[:]%d+[:]%d+[,]%d+.-%d+[:]%d+[:]%d+[,]%d+[\n].-[\n]%d+)") do
        currentLine = currentLine + 1;
        -- print(currentLine, ":", i)

        local timecode_start_str, timecode_end_str, subtitle_str = string.match(i, "(%d+[:]%d+[:]%d+[,]%d+).-(%d+[:]%d+[:]%d+[,]%d+)[\n](.-)[\n][\n]+%d+")
        local timecode_start_num = SplitTimecode(timecode_start_str, fps, removeHour)
        local timecode_end_num = SplitTimecode(timecode_end_str, fps, removeHour)

        if time >= timecode_start_num and time <= timecode_end_num then
            --print(timecode_start_str, timecode_start_num, timecode_end_str, timecode_end_num, subtitle_str)

            return subtitle_str
        end
    end

    return ""
end

function Process(req)
    -- [[ Creates the output. ]]
    local mode = InTimingMode:GetValue(req).Value
    
    local data = InData:GetValue(req).Value
    local index = InIndex:GetValue(req).Value
    local sequenceStartFrame = tonumber(InSequenceStartFrame:GetValue(req).Value)
    local time = tonumber(req.Time) + sequenceStartFrame

    local html = InRemoveHTML:GetValue(req).Value
    local removeHour = InRemoveHour:GetValue(req).Value

    -- Based upon Bryan Ray's MetaTimeCode.fuse improvements
    local fps_combo = InFps:GetValue(req).Value
    local rates = {24, 25, 30, 48, 50, 60, 23.976, 29.97, 59.94}
    local fps = rates[math.min(math.max(fps_combo, 0), 8) + 1]

    -- Normalize line endings
    data = string.gsub(data, '[\r][\n]', '\n')

    -- Make string pattern matching a lot easier
    data = data .. "\n999999"

    -- print("[Data]", data)
    -- print("[index]", index)
    -- print("[fps]", fps)
    -- print("[remove hour]", removeHour)

    local str
    local timecode_start_num, timecode_end_num
    if mode == 0 then
        -- Timecode
        str = SearchByTimecode(data, time, fps, removeHour)
    else
        -- Index
        timecode_start_num, timecode_end_num, str = SearchByIndex(data, index, fps, removeHour)
    end

    -- Remove the HTML tags from the final subtitle text
    if html == 1.0 and str then
        -- print("[HTML Tags]", str)
        str = string.gsub(str, '%b<>', '')
    end

    local out = Text(str)
    OutData:Set(req, out)
end
