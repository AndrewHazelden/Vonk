--[[--
--]]--

-- ============================================================================
-- constants
-- ============================================================================
DATATYPE = "Point"
-- Don't know the proper format
-- REG_VERSION = 
-- Menu structure it'll be displayed in
REGS_CATEGORY = "Kartaverse\\Vonk Ultra\\Point\\Flow"
-- Used to appear in "About" menu
REGS_COMPANY = "Vonk"
-- Used to appear in "About" menu
REGS_HELPTOPIC = "https://gitlab.com/AndrewHazelden/Vonk/"
-- Name displayed in menu
REGS_NAME = "vPointWireless"
-- Description displayed in "About" menu
REGS_OPDESCRIPTION = "vPointWireless"
-- Name inbetween round brackets
REGS_OPICONSTRING = "WirePt"
-- Name as it will appear in the raw comp file
TOOLS_REGID = "vPointWireless"
BTN_WIDTH = 1.0

-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- variables
-- ============================================================================

-- ============================================================================
-- utils
-- ============================================================================

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(TOOLS_REGID, CT_Tool,{
    --REG_Fuse_NoEdit = true,
    --REG_Fuse_NoReload = true,
    --REG_Fuse_TilePic = fuse_pic,
    --REG_NoBlendCtrls = true,
    REG_NoCommonCtrls = true,
    --REG_NoMotionBlurCtrls = true,
    REG_NoPreCalcProcess = true,  -- call Process for precalc requests (instead of PreCalcProcess)
    REG_OpNoMask = true,
    --REG_SupportsDoD = true,   -- this tool supports DoD
    --REG_TimeVariant = true,
    --REG_Unpredictable = true, -- this tool shall never be cached
    --REGID_DataType      = DATATYPE,
    --REGID_InputDataType = DATATYPE,
    REGS_Category = REGS_CATEGORY,
    REGS_Company = REGS_COMPANY,
    REGS_HelpTopic = REGS_HELPTOPIC,
    REGS_Name = REGS_NAME,
    REGS_OpDescription = REGS_OPDESCRIPTION,
    REGS_OpIconString = REGS_OPICONSTRING,
    REGS_IconID = "Icons.Tools.Icons.WirelessLink",
})

function Create()
    local datatype = "Point"

    GetColor = [[
tool.TileColor = tool.Input:GetConnectedOutput():GetTool().TileColor
tool.TextColor = tool.Input:GetConnectedOutput():GetTool().TextColor
    ]]

    GetName = [[
NewName = "wire_" .. tool.Input:GetConnectedOutput():GetTool():GetAttrs().TOOLS_Name
 tool:SetAttrs({TOOLS_Name = NewName})
    ]]

    GetSource = [[
comp:SetActiveTool( tool.Input:GetConnectedOutput():GetTool())
    ]]

    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = datatype,
        INPID_InputControl = "ImageControl",
        LINK_Visible = false,
        --ICD_Width = 0.8,
        LINK_Main = 1, -- if set, you lose the ability to instance the imagecontrol
        --IMGCD_BoxWidth = 0.5,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        INPID_InputControl = "CheckboxControl",
        INP_Default = 0.0,
        INP_DoNotifyChanged = true,
        INP_External = false,
        INP_Integer = true,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
        LINKID_DataType = "Number",
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = datatype,
        LINK_Main = 1,
    })

    GetColorBtn = self:AddInput("Get Color", "GetColorBtn",{
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetColor,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    GetNameBtn = self:AddInput("Get Name", "GetNameBtn", {
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetName,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    GetSourceBtn = self:AddInput("Get Source", "GetSourceBtn", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetSource,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })
end

-- =============================================================================
-- main
-- =============================================================================

function NotifyChanged(inp, param, time)
    --[[
    Handles all input control events.

    :param inp: input that triggered a signal
    :type inp: Input

    :param param: parameter object holding the (new) value
    :type param: Parameter

    :param time: current frame number
    :type time: float
    ]]

    -- trigger callbacks
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then
            visible = true
        else
            visible = false
        end

        InData:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    local data = InData:GetValue(req)

    OutData:Set(req, data)
end
