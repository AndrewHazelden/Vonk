-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayPointsHexagonGrid"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Generate points on Hexagon Grid.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]

    InNumx = self:AddInput("Rows", "rows", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    InNumy = self:AddInput("Colums", "colums", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    InTileXOffset = self:AddInput("Tile X Offset", "tileXOffset", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Default = 0.01,
    })

    InTileYOffset = self:AddInput("Tile Y Offset", "tileYOffset", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Default = 0.1,
    })

    InMul = self:AddInput("Multiplyer", "mul", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -1,
        INP_MaxScale = 1,
        INP_Default = 0.001,
        INP_MinAllowed = -1,
        INP_MaxAllowed = 100,
        INP_Integer = false,
    })

    InDirection = self:AddInput("Direction", "direction", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0.0,
        { MBTNC_AddButton = "Vertical",   MBTNCD_ButtonWidth = 1 / 2, },
        { MBTNC_AddButton = "Horizontal", MBTNCD_ButtonWidth = 1 / 2, },
        INP_Integer = true,
        INP_DoNotifyChanged = true,
    })

    Inline6Separator = self:AddInput("line6Separator", "line6Separator", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)

end

function Process(req)
    -- [[ Creates the output. ]]
    local numx = InNumx:GetValue(req).Value
    local numy = InNumy:GetValue(req).Value
    local mul = InMul:GetValue(req).Value
    local dir = InDirection:GetValue(req).Value
    local result = {}

    local numxIn = numx
    local numyIn = numy
    local flipCorners = false
    local tileXOffsetIn = InTileXOffset:GetValue(req).Value
    local tileYOffsetIn = InTileYOffset:GetValue(req).Value
    local multiplierIn = mul


    local function update()
        local arr = {}

        local offsetX = 0
        local offsetY = 0

        local w = 0
        local h = 0

        local multiplier = multiplierIn

        if dir > 0.5 then
            w = numyIn
            h = numxIn

            offsetX = tileXOffsetIn * 1.7
            offsetY = tileYOffsetIn * 1.432

            for x = 0, w - 1 do
                for y = 0, h - 1 do
                    local yFlipped = y
                    if flipCorners then
                        yFlipped = y + 1
                    end

                    if yFlipped % 2 == 0 then
                        table.insert(arr, (x - w / 2) * offsetX * multiplier)
                        table.insert(arr, (y - h / 2) * offsetY * multiplier)
                        table.insert(arr, 0)
                    else
                        table.insert(arr, ((x - w / 2) * offsetX + offsetX / 2) * multiplier)
                        table.insert(arr, (y - h / 2) * offsetY * multiplier)
                        table.insert(arr, 0)
                    end
                end
            end
        else
            w = numxIn
            h = numyIn

            offsetX = tileYOffsetIn * 1.7
            offsetY = tileXOffsetIn * 1.432

            for x = 0, w - 1 do
                for y = 0, h - 1 do
                    local yFlipped = y
                    if flipCorners then
                        yFlipped = y + 1
                    end

                    if yFlipped % 2 == 0 then
                        table.insert(arr, { (y - h / 2) * offsetY * multiplier, (x - w / 2) * offsetX * multiplier, 0 })
                    else
                        table.insert(arr,
                            { (y - h / 2) * offsetY * multiplier, ((x - w / 2) * offsetX + offsetX / 2) * multiplier, 0 })
                    end
                end
            end
        end

        return arr
    end

    result = update()

    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
