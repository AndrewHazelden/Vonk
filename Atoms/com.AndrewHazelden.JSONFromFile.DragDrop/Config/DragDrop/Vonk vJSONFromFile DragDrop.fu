--[[--
Vonk vJSONFromFile DragDrop.fu - v2.1 2025-02-03 06.04 AM 
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview
----------
The "Vonk vJSONFromFile DragDrop.fu" file allows you to import a .json file by dragging it into the Nodes view from a desktop Explorer/Finder/Linux folder browsing window. This is a quick way to bring external data records into your Resolve/Fusion composite. The DragDrop file supports dragging in multiple JSON elements at the same time, and each item will be imported into a separate JSONFromFile node.

If the JSON file is a Kartaverse Lens Profile .json then a kvrFisheyeStereo node is added automatically.

If the JSON file is a Kartaverse Comp Session .json then a pre-existing MDI (multi-document interface) compositing workspace session is restored where the .comp files are loaded automatically and the active node based selection is restored.

Usage
Step 1. After you install the "Vonk vJSONFromFile DragDrop.fu" package from Reactor, you will need to restart the Fusion program once so the new .fu file is loaded during Fusion's startup phase.

Step 2. Select a .json file in an Explorer (Win), Finder (macOS), or Linux desktop folder browsing window.

Step 3. Drag the .json file to the Fusion/Resolve Nodes view. The data will be automatically imported into your foreground composite.


1-Click Installation
---------------------
Install the "Vonk DragDrop | vJSONFromFile" atom package via the Reactor package manager.

This will install the "Vonk vJSONFromFile DragDrop.fu" file into the "Config:/DragDrop/" PathMap folder (Reactor:/Deploy/Config/DragDrop/).

Fusion Standalone Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/Fusion/Config/DragDrop/

	On MacOS this works out to:
		$HOME/Library/Application Support/Blackmagic Design/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.

Resolve Fusion Page Manual "Config:/" Install:
	On Windows this PathMap works out to:
		%AppData%\Blackmagic Design\DaVinci Resolve\Support\Fusion\Config\DragDrop\

	On Linux this PathMap works out to:
		$HOME/.fusion/BlackmagicDesign/DaVinci Resolve/Support/Fusion/Config/DragDrop/

	On MacOS this PathMap works out to:
		$HOME/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Config/DragDrop/

*Note: In a manual install of this tool you will have to create the final "DragDrop" folder manually as it won't exist in advance.

Todo
------
- Figure out the "args.ShiftModifier" equivalent variable name for detecting hotkeys from .fu events.
- Add bmd.parseFilename() code here to start tokenizing the filename string and frame number element
- Add support for handling IFL filenames in the DragDrop module, and in the vJSONFromFile fuse

--]]--

{
	Event{
		Action = 'Drag_Drop',
		Targets = {
			FuView = {
				Execute = _Lua [=[
-- Check if the file extension matches
-- Example: isComp = MatchExt('/Example.json', '.json')
function MatchExt(file, fileType)
	-- Get the file extension
	local ext = string.match(tostring(file), '^.+(%..+)$')

	-- Compare the results
	if ext == tostring(fileType) then
		return true
	else
		return false
	end
end


-- Get the current comp object
-- Example: comp = GetCompObject()
function GetCompObject()
	local cmp = app:GetAttrs().FUSIONH_CurrentComp
	return cmp
end


-- Process a file dropped into Fusion
-- Example: ProcessFile('/Example.json', 1)
function ProcessFile(file, fileNum)
	-- comp:Print('[Vonk][vJSONFromFile]['.. fileNum .. '][File Drop] ', file, '\n')

	-- Check if the file extension matches
	if MatchExt(file, '.json') then
		-- Accept the Drag and Drop event
		rets.handled = true

		-- Get the current comp object
		comp = GetCompObject()
		if not comp then
			-- The comp pointer is undefined
			comp:Print('[Vonk][vJSONFromFile] Please open a Fusion composite before dragging in a .json file again.')
			return
		end

		---- Todo: Get the shift hotkey modifier working
		--if args.shiftModifier then
		-- Check for a shift key press to enable the alternative exr import option
		-- ...
		--end

		-- Lock the comp to suppress any file dialog opening for nodes that have empty filename fields.
		-- comp:Print('[Locking Comp]')
		comp:Lock()

		local json = require("dkjson")
		local fp = io.open(file, "r")
		if fp == nil then
			error(string.format("file does not exist: %s", selectedFilePath))
		end
		local json_str = fp:read("*all")
		fp:close()
		tbl = json.decode(json_str)
		if tbl and type(tbl) == "table" then
			-- table.sort(tbl)
			if tbl and tbl.Name and tbl.Name == "kvrFisheyeStereo" then
				print("\n\n[Load kvrFisheyeStereo]\n")
				-- Add the kvrFisheyeStereo macro node to the comp
				obj:Comp():DoAction("AddSetting", {filename = "Macros:/KartaVP/Warp/kvrFisheyeStereo.setting"})
				local tool = obj:Comp().ActiveTool
				if tool and tool:GetID() == "GroupOperator" then
					tool:SetInput("Camera", tbl.Params.Camera, comp.CurrentTime)
					tool:SetInput("Lens", tbl.Params.Lens, comp.CurrentTime)
					tool:SetInput("ViewMode", tbl.Params.ViewMode, comp.CurrentTime)
					tool:SetInput("Anaglyph", tbl.Params.Anaglyph, comp.CurrentTime)
					tool:SetInput("FieldOfView", tbl.Params.FieldOfView, comp.CurrentTime)
					tool:SetInput("Convergence", tbl.Params.Convergence, comp.CurrentTime)
					tool:SetInput("CircularMaskDiameter", tbl.Params.CircularMaskDiameter, comp.CurrentTime)
					tool:SetInput("CircularMaskSoftness", tbl.Params.CircularMaskSoftness, comp.CurrentTime)
					tool:SetInput("Exposure1", tbl.Params.Exposure1, comp.CurrentTime)
					tool:SetInput("Gamma1", tbl.Params.Gamma1, comp.CurrentTime)
					tool:SetInput("Saturation1", tbl.Params.Saturation1, comp.CurrentTime)
					tool:SetInput("Vibrance1", tbl.Params.Vibrance1, comp.CurrentTime)
					tool:SetInput("Exposure2", tbl.Params.Exposure2, comp.CurrentTime)
					tool:SetInput("Gamma2", tbl.Params.Gamma2, comp.CurrentTime)
					tool:SetInput("Saturation2", tbl.Params.Saturation2, comp.CurrentTime)
					tool:SetInput("Vibrance2", tbl.Params.Vibrance2, comp.CurrentTime)
					tool:SetInput("Center1", tbl.Params.Center1, comp.CurrentTime)
					tool:SetInput("Width1", tbl.Params.Width1, comp.CurrentTime)
					tool:SetInput("Height1", tbl.Params.Height1, comp.CurrentTime)
					tool:SetInput("Angle1", tbl.Params.Angle1, comp.CurrentTime)
					tool:SetInput("Center2", tbl.Params.Center2, comp.CurrentTime)
					tool:SetInput("Width2", tbl.Params.Width2, comp.CurrentTime)
					tool:SetInput("Height2", tbl.Params.Height2, comp.CurrentTime)
					tool:SetInput("Angle2", tbl.Params.Angle2, comp.CurrentTime)
					tool:SetInput("a", tbl.Params.a, comp.CurrentTime)
					tool:SetInput("b", tbl.Params.b, comp.CurrentTime)
					tool:SetInput("c", tbl.Params.c, comp.CurrentTime)
					tool:SetInput("cx", tbl.Params.cx, comp.CurrentTime)
					tool:SetInput("cy", tbl.Params.cy, comp.CurrentTime)
					tool:SetInput("XShift", tbl.Params.XShift, comp.CurrentTime)
					tool:SetInput("YShift", tbl.Params.YShift, comp.CurrentTime)
					tool:SetInput("STMapWidth", tbl.Params.STMapWidth, comp.CurrentTime)
					tool:SetInput("STMapHeight", tbl.Params.STMapHeight, comp.CurrentTime)
					
					comp:Print("[Vonk][kvrFisheyeStereo][Lens Profile] " .. tostring(file).. "\n")
					comp:Print("[Camera] " .. tostring(tbl.Params.Camera).. " [Lens] " .. tostring(tbl.Params.Lens).. "\n")
				else
					comp:Print("[Vonk][kvrFisheyeStereo] Warning: The currently selected node is not GroupOperator macro.\n")
				end
			elseif tbl and tbl.Name and tbl.Name == "Sessions" then
				print("\n\n[Load Session]\n")
				-- Save the session preference
				print("[Sessions][Default Pref] " .. tostring(file))
				app:SetData("Kartaverse.Sessions.Default", file)
				
				-- Cleanup the currently open comps
				print("[Closing the Existing Comps]\n")
				compList = app:GetCompList()
				if #compList >= 1 then
					for i = 1, #compList do
						-- Set cmp to the pointer of the current composite
						cmp = compList[i]
					
						-- Verify this comp isn't the active foreground comp
						if cmp then
							-- Print out the active composite name
							print("\t[" .. cmp:GetAttrs()["COMPS_Name"] .. "]\n")
					
							-- Force a save of the comp
							cmp:Save()
					
							-- If the comp is unlocked, it will ask if the comp should be saved before closing.
							cmp:Unlock()
					
							-- Close the active comp
							cmp:Close()
						end
					end
				end
				if tbl.Array then
					for k, v in pairs(tbl.Array) do
						if v and v.Filename then
							local compFile = app:MapPath(v.Filename)
							if bmd.fileexists(compFile) then
								-- Open the comp in Fusion
								app:LoadComp(compFile, false, false, false)
								print("-------------------------------------------------")
								print("[Sessions][Filename] " .. tostring(compFile))
		
								-- Give a momentary delay
								bmd.wait(0.1)
								local cmp = fusion:GetCurrentComp()
								
								-- Select the node
								if cmp and v.ActiveTool then
									cmp:SetActiveTool(cmp:FindTool(v.ActiveTool))
								else
									print("[Sessions][ActiveTool] No ActiveTool")
								end
		
								-- Restore the viewers:
								if cmp and v.LeftView and cmp:FindTool(v.LeftView) then
									print("[Sessions][LeftView] " .. tostring(v.LeftView))
									cmp:GetPreviewList().LeftView:ViewOn(cmp:FindTool(v.LeftView), 1)
								else
									print("[Sessions][LeftView] No LeftView active")
								end
								if cmp and v.RightView and cmp:FindTool(v.RightView) then
									print("[Sessions][RightView] " .. tostring(v.RightView))
									cmp:GetPreviewList().RightView:ViewOn(cmp:FindTool(v.RightView), 1)
								else
									print("[Sessions][RightView] No RightView active")
								end
								
								-- Set the Playhead CurrentFrame position
								if cmp and v.CurrentTime then
									print("[Sessions][CurrentFrame] " .. tonumber(v.CurrentTime))
									cmp.CurrentTime = tonumber(v.CurrentTime)
								else
									print("[Sessions][CurrentFrame] No CurrentFrame defined\n")
								end
							else
								print("[Sessions][Filename] Warning: The file is missing: " .. tostring(compFile))
							end
						else
							print("[Sessions][Filename] Warning: The JSON Filename tag is missing\n")
						end
					end
				end
			else
				-- Add the JSONFromFile node to the comp
				print("\n\n[Load JSONFromFile]\n")
				local read = comp:AddTool('Fuse.vJSONFromFile', -32768, -32768)
		
				-- Todo: Add bmd.parseFilename() code here to start tokenizing the filename string and frame number element
		
				-- Check for a nil on the node creation and the filename string
				if read and file then
					-- read.Filename = tostring(file)
					read.Input = tostring(file)
				else
					comp:Print("[Vonk][vJSONFromFile] Warning: Failed to update the vJSONFromFile node filename.\n")
				end
			end
		else
			comp:Print("[Json DragDrop] There was a json formatting issue with the Lua table output.\n")
		end

		-- Unlock the comp to restore "normal" file dialog opening operations
		-- comp:Print('[Unlock Comp]')
		comp:Unlock()
	end
end


-- Where the magic begins
function Main()
	-- Call other chained events and default action
	rets = self:Default(ctx, args)

	-- Debug print the args
	-- dump(args)

	-- Drop zone screen coordinates
	mousex = args._sxpos
	mousey = args._sypos

	-- No one else handled this?
	if not rets.handled then
		-- Get the list of files dropped onto Fusion
		files = args.urilist

		-- Scan through each of the files
		for i, file in ipairs(files) do
			-- Process a .json file dropped into Fusion
			ProcessFile(file, i)
		end
	end

	-- Debug print where the file was dropped onscreen in the window (using screen coordinates)
	-- print('[Drop Zone Coords] [X] ' .. tostring(mousex) .. ' [Y] ' .. tostring(mousey) .. 'px')
	-- print('\n')
end

-- Run the main function
Main()
]=],
			},
		},
	},
}
