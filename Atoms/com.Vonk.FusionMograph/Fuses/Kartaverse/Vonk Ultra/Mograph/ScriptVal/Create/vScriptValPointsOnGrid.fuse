-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValPointsOnGrid"
DATATYPE = "ScriptVal"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\ScriptVal\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Generate point on Array Grid.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]

    InNumx = self:AddInput("Number X", "numx", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    InNumy = self:AddInput("Number Y", "numy", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })
    InNumz = self:AddInput("Number Z", "numz", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 0,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    InMul = self:AddInput("Multiplyer", "mul", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -1,
        INP_MaxScale = 1,
        INP_Default = 0.05,
        INP_MinAllowed = -1,
        INP_MaxAllowed = 1,
        INP_Integer = false,
    })


    Inline6Separator = self:AddInput("line6Separator", "line6Separator", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    OutScriptValData = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)

end

function Process(req)
    -- [[ Creates the output. ]]
    local numx = InNumx:GetValue(req).Value
    local numy = InNumy:GetValue(req).Value
    local numz = InNumz:GetValue(req).Value
    local mul = InMul:GetValue(req).Value

    local result = {}

    local subX = 0
    local subY = 0
    local subZ = 0
    local center = true

    if center then
        subX = ((numx - 1) * mul) / 2.0
        subY = ((numy - 1) * mul) / 2.0
        subZ = ((numz - 1) * mul) / 2.0
    end

    local xTemp = 0
    local yTemp = 0
    local zTemp = 0

    local m = mul

    for z = 0, numz - 1 do
        zTemp = (z * m) - subZ

        for y = 0, numy - 1 do
            yTemp = (y * m) - subY

            for x = 0, numx - 1 do
                xTemp = (x * m) - subX

                table.insert(result, { xTemp, yTemp, zTemp })
            end
        end
    end


    OutScriptValData:Set(req, ScriptValParam(result))
end
