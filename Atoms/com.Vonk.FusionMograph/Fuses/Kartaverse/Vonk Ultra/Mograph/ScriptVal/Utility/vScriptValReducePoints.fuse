-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValReducePoints"
DATATYPE = "ScriptVal"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\ScriptVal\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reduce points Operations on a ScriptVal.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]

    InDecimationMethod = self:AddInput("Decimation Method", "DecimationMethod", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0.0,
        { MBTNC_AddButton = "Grid-Based",             MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "Random Sampling",        MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "Curvature-Based (SLOW)", MBTNCD_ButtonWidth = 1 / 3, },
        INP_Integer = true,
        INP_DoNotifyChanged = true,
    })
    InGridSize = self:AddInput("Grid Size", "GridSize", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -1,
        INP_MaxScale = 1,
        INP_Default = 0.01,
        INP_MinAllowed = 0.0,
        INP_MaxAllowed = 1e+38,
        LINK_Main = 4,

    })
    InTargetCount = self:AddInput("Target Count", "targetCount", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 10,
        INP_Default = 3.0,
        INP_MinAllowed = 0.0,
        INP_MaxAllowed = 1e+38,
        LINK_Main = 2,
    })

    InNeighborhoodSize = self:AddInput("Neighborhood Size", "neighborhoodSize", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.01,
        INP_MaxScale = 1,
        INP_Default = 0.2,
        INP_MinAllowed = 0.0,
        INP_MaxAllowed = 1e+38,
        LINK_Main = 5
    })

    Inline4Separator = self:AddInput("line4Separator", "line4Separator", {
        INPID_InputControl = "SeparatorControl",
        IC_Visible = true,
        INP_External = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InDecimationMethod then
        if param.Value == 0.0 then
            InGridSize:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InTargetCount:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InNeighborhoodSize:SetAttrs({ IC_Visible = false, PC_Visible = false })
        elseif param.Value == 1.0 then
            InGridSize:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InTargetCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InNeighborhoodSize:SetAttrs({ IC_Visible = false, PC_Visible = false })
        elseif param.Value == 2.0 then
            InGridSize:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InTargetCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InNeighborhoodSize:SetAttrs({ IC_Visible = true, PC_Visible = true })
        end
    end

    if inp == InShowInput then
        if param.Value == 1.0 then
            InGridSize:SetAttrs({ LINK_Visible = true })
            InTargetCount:SetAttrs({ LINK_Visible = true })
            InNeighborhoodSize:SetAttrs({ LINK_Visible = true })
        else
            InGridSize:SetAttrs({ LINK_Visible = false })
            InTargetCount:SetAttrs({ LINK_Visible = false })
            InNeighborhoodSize:SetAttrs({ LINK_Visible = false })
        end
    end
end

-- ============================================================================
-- Function to reduce points using grid-based decimation
local function reducePoints(points, gridSize)
    local grid = {}          -- Hash table to store grid cells
    local reducedPoints = {} -- Resulting reduced points

    for _, point in ipairs(points) do
        -- Calculate grid cell coordinates
        local x = math.floor(point[1] / gridSize)
        local y = math.floor(point[2] / gridSize)
        local z = math.floor(point[3] / gridSize)

        -- Use the grid cell as a key
        local key = x .. "," .. y .. "," .. z

        -- If the cell is empty, add the point to the result
        if not grid[key] then
            grid[key] = true
            table.insert(reducedPoints, point)
        end
    end

    return reducedPoints
end

-- ============================================================================
-- Function to reduce points using random sampling
local function reducePointsRandom(points, targetCount)
    local reducedPoints = {}
    local totalPoints = #points

    -- Ensure targetCount is not greater than the total number of points
    targetCount = math.min(targetCount, totalPoints)

    -- Create a list of indices and shuffle it
    local indices = {}
    for i = 1, totalPoints do
        indices[i] = i
    end
    for i = totalPoints, 2, -1 do
        local j = math.random(1, i)
        indices[i], indices[j] = indices[j], indices[i]
    end

    -- Select the first `targetCount` indices
    for i = 1, targetCount do
        table.insert(reducedPoints, points[indices[i]])
    end

    return reducedPoints
end

-- ============================================================================
-- Function to calculate Euclidean distance between two points
local function distance(p1, p2)
    return math.sqrt((p1[1] - p2[1]) ^ 2 + (p1[2] - p2[2]) ^ 2 + (p1[3] - p2[3]) ^ 2)
end

-- ============================================================================
-- Function to calculate curvature for each point
local function calculateCurvature(points, neighborhoodSize)
    local curvature = {}
    for i, point in ipairs(points) do
        local totalDistance = 0
        local count = 0

        -- Find neighboring points within a radius
        for j, neighbor in ipairs(points) do
            if i ~= j and distance(point, neighbor) < neighborhoodSize then
                totalDistance = totalDistance + distance(point, neighbor)
                count = count + 1
            end
        end

        -- Average distance to neighbors is a simple curvature proxy
        curvature[i] = count > 0 and (totalDistance / count) or 0
    end
    return curvature
end

-- ============================================================================
-- Function to reduce points based on curvature
local function reducePointsCurvature(points, targetCount, neighborhoodSize)
    local curvature = calculateCurvature(points, neighborhoodSize)
    local reducedPoints = {}

    -- Sort points by curvature (higher curvature = more important)
    local sortedIndices = {}
    for i = 1, #points do
        sortedIndices[i] = i
    end
    table.sort(sortedIndices, function(a, b)
        return curvature[a] > curvature[b]
    end)

    -- Select the top `targetCount` points with highest curvature
    for i = 1, math.min(targetCount, #points) do
        table.insert(reducedPoints, points[sortedIndices[i]])
    end

    return reducedPoints
end



function Process(req)
    local arrayA = InScriptVal:GetValue(req):GetValue() or {}

    local gridSize = InGridSize:GetValue(req).Value
    local form = InDecimationMethod:GetValue(req).Value
    local targetCount = InTargetCount:GetValue(req).Value
    local neighborhoodSize = InNeighborhoodSize:GetValue(req).Value

    local reducedPoints = {}

    --Todo add a 2D version

    if form == 0 then
        reducedPoints = reducePoints(arrayA, gridSize)
    elseif form == 1 then
        -- Set target number of points (adjust as needed)
        reducedPoints = reducePointsRandom(arrayA, targetCount)
    elseif form == 2 then
        -- Set target number of points and neighborhood size (adjust as needed)
        -- reducedPoints = reducePointsCurvature(arrayA, targetCount, neighborhoodSize)
        local gridReducedPoints = reducePoints(arrayA, gridSize)
        reducedPoints = reducePointsCurvature(gridReducedPoints, targetCount, neighborhoodSize)
    end

    OutScriptVal:Set(req, ScriptValParam(reducedPoints))
end
