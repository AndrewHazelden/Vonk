-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayPointsOnArc"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Modify",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Generate points on an arc.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]

    InSmoothness = self:AddInput("Smoothness", "smoothness", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 20,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
    })

    InHeightFactor = self:AddInput("Height Factor", "heightFactor", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.001,
        INP_MaxScale = 1,
        INP_Default = 0.0,
        IC_Visible = false,
        PC_Visible = false
    })

    InMul = self:AddInput("Multiplyer", "mul", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -1,
        INP_MaxScale = 1,
        INP_Default = 0.0,
        IC_Visible = false,
        PC_Visible = false
    })

    Inline6Separator = self:AddInput("line6Separator", "line6Separator", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    InArray = self:AddInput("Array A", "ArrayA", {
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 1,
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)

end

function Process(req)
    local arrayA = jsonutils.decode(InArray:GetValue(req).Value).array or {}
    local result = {}

    local mul = InMul:GetValue(req).Value
    local smoothness = InSmoothness:GetValue(req).Value
    local heightFactor = InHeightFactor:GetValue(req).Value

    function distance(p1, p2)
        return math.sqrt((p2[1] - p1[1]) ^ 2 + (p2[2] - p1[2]) ^ 2 + (p2[3] - p1[3]) ^ 2)
    end

    function midpoint(p1, p2)
        return { (p1[1] + p2[1]) / 2, (p1[2] + p2[2]) / 2, (p1[3] + p2[3]) / 2 }
    end

    function crossProduct(v1, v2)
        return {
            v1[2] * v2[3] - v1[3] * v2[2],
            v1[3] * v2[1] - v1[1] * v2[3],
            v1[1] * v2[2] - v1[2] * v2[1]
        }
    end

    function normalize(v)
        local magnitude = math.sqrt(v[1] ^ 2 + v[2] ^ 2 + v[3] ^ 2)
        return { v[1] / magnitude, v[2] / magnitude, v[3] / magnitude }
    end

    function calculateArcPoints(p1, p2, segments, heightFactor, mul)
        local arcPoints = {}
        local d = distance(p1, p2)
        local r = (d / 2) * heightFactor

        local mid = midpoint(p1, p2)

        local direction = { p2[1] - p1[1], p2[2] - p1[2], p2[3] - p1[3] }
        local normal = crossProduct(direction, { 0, 0, 1 })
        if normal[1] == 0 and normal[2] == 0 and normal[3] == 0 then
            normal = crossProduct(direction, { 0, 1, 0 })
        end
        normal = normalize(normal)

        local center = {
            mid[1] + normal[1] * r,
            mid[2] + normal[2] * r,
            mid[3] + normal[3] * r
        }


        table.insert(arcPoints, p1) -- Ensure the arc starts at p1

        for i = 1, segments - 1 do
            local theta = math.pi * i / segments
            local x = math.cos(theta) * (p1[1] - center[1]) - math.sin(theta) * (p1[2] - center[2]) + center[1]
            local y = math.sin(theta) * (p1[1] - center[1]) + math.cos(theta) * (p1[2] - center[2]) + center[2]
            local z = math.sin(theta) * (p1[3] - center[3]) + math.cos(theta) * (p1[3] - center[3]) + center[3]

            local heightAdjust = (1 - math.abs(1 - 2 * (i / segments))) * heightFactor * d
            z = z + heightAdjust

            table.insert(arcPoints, { x, y, z })
        end

        table.insert(arcPoints, p2) -- Ensure the arc ends at p2

        return arcPoints
    end

    local allArcPoints = {}
    for i = 1, #arrayA - 1 do
        local arcPoints = calculateArcPoints(arrayA[i], arrayA[i + 1], smoothness, heightFactor, mul)
        for _, pt in ipairs(arcPoints) do
            table.insert(allArcPoints, pt)
        end
    end

    result = allArcPoints
    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
