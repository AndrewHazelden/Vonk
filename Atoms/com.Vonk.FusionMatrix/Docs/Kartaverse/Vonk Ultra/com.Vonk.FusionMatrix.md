# Vonk Ultra - Matrix

Matrix is a node based matrix library for Blackmagic Design Fusion.

## Acknowledgements

- Kristof Indeherberge
- Cédric Duriau

## Requirements

- [lua-matrix](https://github.com/davidm/lua-matrix)

## Contents

**Fuses**

- `vCreateMatrix.fuse`: Fuse to create a 4x4 matrix.
- `vMatrixFromArray.fuse`: Fuse to create a matrix from an array stored as JSON string.
- `vMatrixInvert.fuse`: Fuse to invert a matrix.
- `vMatrixMultiply.fuse`: Fuse to multiply two matrices.
- `vMatrixTranspose.fuse`: Fuse to transpose a matrix.
- `vMatrixConcatenateHorizontal.fuse`: Fuse to concatenate two matrices horizontally.
- `vMatrixConcatenateVertical.fuse`: Fuse to concatenate two matrices vertically.
- `vReadMatrix.fuse`: Fuse to read a matrix from metadata of an image.
- `vWriteMatrix.fuse`: Fuse to write a matrix to metadata of an image.

**Modules/Lua**

- `matrixutils.lua`: Core module using lua-matrix.
