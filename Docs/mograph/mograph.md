# Vonk Ultra - Motion Graphics Tools - Beta

The vMograph tools are the next generation of Vonk Ultra technology that help power dynamic motion graphics.

Check out the [Dunn by Dunn Instagram channel](https://www.instagram.com/done__by__dunn) for inspiration!

## Acknowledgements

- Dunn Lewis
- Andrew Hazelden

## Requirements

- Vonk Ultra
- com.DunnLewis.Extended_Wave
- com.RXI.Flux
- com.RXI.Lume
- com.wesuckless.Wave

# Contents

## Array

### Create

- vArrayCircularPoints
- vArrayCreate
- vArrayCreateList
- vArrayCreateMinMax
- vArrayCreateRandom
- vArrayFromAudio
- vArrayFromOBJ
- vArrayFromXYZ
- vArrayPointsOnCircle
- vArrayPointsOnGrid

### Modify

- vArrayBuffer
- vArrayCameraProjection
- vArrayInterpolate
- vArrayLogicBetween
- vArrayMapRange
- vArrayMath
- vArrayPacker
- vArrayRotateValues
- vArraySin
- vArrayTransform
- vArrayUnPacker
- vArrayWave

### ShapeRender

- vArrayCreatePolylineDots
- vArrayCreatePolylineDotsColor
- vArrayRenderWireframes

### Temporal

- vArrayAccumulator
- vArrayAccumulatorOBJ

### Utility
- vArrayAppend
- vArrayAppendGroup
- vArrayInfo
- vArrayMerge
- vArrayMergeOBJ
- vArraySlice

## HTML

- vHTML_UIControl_Color
- vHTML_UIControl_Hero

## JSON

### Create

- vJSONCircularPoints
- vJSONCreate
- vJSONCreateJSONFont
- vJSONCreateList
- vJSONCreateMinMax
- vJSONCreateRandom
- vJSONCreateTextFont
- vJSONFromOBJ
- vJSONFromXYZ
- vJSONGenerateSphere
- vJSONLissajouseSpline
- vJSONMapGeoCoordsSpherical
- vJSONMapGeoJSON
- vJSONMercatorCoord
- vJSONMercatorCoordsArray
- vJSONPhyllotaxis
- vJSONPointParticles
- vJSONPointsHexagonGrid
- vJSONPointsOnCircle
- vJSONPointsOnCube
- vJSONPointsOnGrid
- vJSONPointsOnRectangle
- vJSONPointsOnSphere

### Logic

- vJSONLogicBetween

### Modify

- vJSONArrayIterator
- vJSONCameraProjection
- vJSONConvert2D-3D
- vJSONDisplaceValues
- vJSONInterpolate
- vJSONInterpolateRandom
- vJSONMapRange
- vJSONMath
- vJSONPacker
- vJSONParallelPointsOffSet
- vJSONPerlin3Noise
- vJSONPointsConnect
- vJSONPointsOnArc
- vJSONRotateValues
- vJSONSin
- vJSONSortByDistance
- vJSONTranslate
- vJSONUnPacker
- vJSONWave

## Utility

- vJSONAppend
- vJSONAppendGroup
- vJSONBuffer
- vJSONInfo
- vJSONMerge
- vJSONReducePoints
- vJSONSlice

## Number

### Modify

- vNumberSchlickBias

### Trigonometry

- vNumberCircleCoordinates
- vNumberDegreeToVector

### Utility

- vNumberSmootherStep
- vNumberTween
- vNumberWave

## ScriptVal

### Create

- vScriptValCreateList

### Modify

- vScriptValCameraProjection
- vScriptValMath
- vScriptValPacker
- vScriptValRotateValues
- vScriptValTransform
- vScriptValUnPacker

### ShapeRender

- vScriptValCreatePolylineDots
- vScriptValCreatePolylineDotsColor
- vScriptValRenderWireframes

### Shapes

- vScriptValCustom3DShapes
