1
00:00:00,220 --> 00:00:01,219
Hi.

2
00:00:01,219 --> 00:00:02,448
My name is Charlie.

3
00:00:02,450 --> 00:00:04,878
It's been a while but it's great to be back.

4
00:00:04,878 --> 00:00:07,489
Now, let me ask you a question.

5
00:00:07,490 --> 00:00:09,230
Are you a Fusion user?

6
00:00:09,230 --> 00:00:14,079
Do you enjoy working with Fusion,
creating wonderful motion graphics and state

7
00:00:14,080 --> 00:00:15,609
of the art visual effects?

8
00:00:15,609 --> 00:00:18,068
Yeah, I know you do.

9
00:00:18,070 --> 00:00:22,659
But do you sometimes get frustrated when you
spend hours and hours searching the internet

10
00:00:22,660 --> 00:00:27,868
for that one special setting, macro, or fuse
only to discover that the link no longer works?

11
00:00:27,868 --> 00:00:33,159
Wouldn't it be great if there was one place
where things things would indeed suck less?

12
00:00:33,159 --> 00:00:37,479
Well, sometimes wishes do come true thanks
to Reactor.

13
00:00:37,479 --> 00:00:42,049
A new revolutionary tool from the wonderful
folks at steakunderwater.com

14
00:00:42,049 --> 00:00:46,299
The friendliest BlackMagic Fusion Studio community
online.

15
00:00:46,299 --> 00:00:47,409
That's right.

16
00:00:47,409 --> 00:00:51,009
All of the tools, tips, and tricks for which
you've had to search the internet

17
00:00:51,009 --> 00:00:56,039
Wade through hundreds of forum posts, dig
through endless wiki pages, or email your

18
00:00:56,039 --> 00:00:59,668
long lost friends for
are now available under one roof

19
00:00:59,670 --> 00:01:03,199
One mouse click away
Installation is easy

20
00:01:03,200 --> 00:01:07,569
Just drag and drop the install script on your
Console and Reactor will do all the work

21
00:01:07,569 --> 00:01:11,769
Providing you with a clean and easy to use
interface to select and install exactly the

22
00:01:11,769 --> 00:01:14,499
content you want and need.

23
00:01:14,500 --> 00:01:17,290
A new Fusion install on a brand new machine?

24
00:01:17,290 --> 00:01:21,089
Just install Reactor and quickly install all
the additional tools you need.

25
00:01:21,090 --> 00:01:22,090
Click.

26
00:01:22,090 --> 00:01:23,090
Click.

27
00:01:23,090 --> 00:01:24,309
It is that simple.

28
00:01:24,310 --> 00:01:25,310
But wait.

29
00:01:25,310 --> 00:01:26,310
There is more!

30
00:01:26,310 --> 00:01:29,569
Normally a tool like this would cost hundreds,
maybe thousands of dollars

31
00:01:29,569 --> 00:01:34,359
But here at We Suck Less you can get this
incredible tool for the low low price of

32
00:01:34,359 --> 00:01:36,488
nothing.

33
00:01:36,489 --> 00:01:37,489
That's right.

34
00:01:37,489 --> 00:01:39,329
Reactor is free and open source.

35
00:01:39,329 --> 00:01:42,858
A gift from the community, to the community.

36
00:01:42,859 --> 00:01:47,609
So don't wait any longer
Go to www.steakunderwater.com

37
00:01:47,609 --> 00:01:53,439
Join the fastest growing Fusion community
online and download Reactor now!

