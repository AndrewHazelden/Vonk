# Vonk Ultra - Motion Graphics Tools - Beta

The vMograph tools are the next generation of Vonk Ultra technology that help power dynamic motion graphics.

Check out the [Dunn by Dunn Instagram channel](https://www.instagram.com/done__by__dunn) for inspiration!

## Acknowledgements

- Dunn Lewis
- Andrew Hazelden

## Requirements

- Vonk Ultra
- com.DunnLewis.Extended_Wave
- com.RXI.Flux
- com.RXI.Lume
- com.wesuckless.Wave

# Contents

## Array

### Create

- vArrayCircularPoints
- vArrayCreate
- vArrayCreateList
- vArrayCreateMinMax
- vArrayCreateRandom
- vArrayFromAudio
- vArrayFromOBJ
- vArrayFromXYZ
- vArrayPointsOnCircle
- vArrayPointsOnGrid

### Modify

- vArrayBuffer
- vArrayCameraProjection
- vArrayInterpolate
- vArrayLogicBetween
- vArrayMapRange
- vArrayMath
- vArrayPacker
- vArrayRotateValues
- vArraySin
- vArrayTransform
- vArrayUnPacker
- vArrayWave

### ShapeRender

- vArrayCreatePolylineDots
- vArrayCreatePolylineDotsColor
- vArrayRenderWireframes

### Temporal

- vArrayAccumulator
- vArrayAccumulatorOBJ

### Utility
- vArrayAppend
- vArrayAppendGroup
- vArrayInfo
- vArrayMerge
- vArrayMergeOBJ
- vArraySlice

## HTML

- vHTML_UIControl_Color
- vHTML_UIControl_Hero

## JSON

### Create

- vJSONCircularPoints
- vJSONCreate
- vJSONCreateJSONFont
- vJSONCreateList
- vJSONCreateMinMax
- vJSONCreateRandom
- vJSONCreateTextFont
- vJSONFromOBJ
- vJSONFromXYZ
- vJSONGenerateSphere
- vJSONLissajouseSpline
- vJSONMapGeoCoordsSpherical
- vJSONMapGeoJSON
- vJSONMercatorCoord
- vJSONMercatorCoordsArray
- vJSONPhyllotaxis
- vJSONPointParticles
- vJSONPointsHexagonGrid
- vJSONPointsOnCircle
- vJSONPointsOnCube
- vJSONPointsOnGrid
- vJSONPointsOnRectangle
- vJSONPointsOnSphere

### Logic

- vJSONLogicBetween

### Modify

- vJSONArrayIterator
- vJSONCameraProjection
- vJSONConvert2D-3D
- vJSONDisplaceValues
- vJSONInterpolate
- vJSONInterpolateRandom
- vJSONMapRange
- vJSONMath
- vJSONPacker
- vJSONParallelPointsOffSet
- vJSONPerlin3Noise
- vJSONPointsConnect
- vJSONPointsOnArc
- vJSONRotateValues
- vJSONSin
- vJSONSortByDistance
- vJSONTranslate
- vJSONUnPacker
- vJSONWave

## Utility

- vJSONAppend
- vJSONAppendGroup
- vJSONBuffer
- vJSONInfo
- vJSONMerge
- vJSONReducePoints
- vJSONSlice

## Number

### Modify

- vNumberSchlickBias

### Trigonometry

- vNumberCircleCoordinates
- vNumberDegreeToVector

### Utility

- vNumberSmootherStep
- vNumberTween
- vNumberWave

## ScriptVal

### Create

- vScriptValCreateList

### Modify

- vScriptValCameraProjection
- vScriptValMath
- vScriptValPacker
- vScriptValRotateValues
- vScriptValTransform
- vScriptValUnPacker

### ShapeRender

- vScriptValCreatePolylineDots
- vScriptValCreatePolylineDotsColor
- vScriptValRenderWireframes

### Shapes

- vScriptValCustom3DShapes

# Node Descriptions

## vArrayCircularPoints

Distributes points in circular form

## vArrayCreate

Creates an Array

## vArrayCreateList

Creates an Array

## vArrayCreateMinMax

Creates Array based on Max/Min operations

## vArrayCreateRandom

Creates a Random Array

## vArrayFromAudio

Convert .wav audio data into a vArray format

## vArrayFromOBJ

Convert Wavefront OBJ mesh data into a vArray format

## vArrayFromXYZ

Convert ASCII XYZ point cloud data into a vArray format

## vArrayPointsOnCircle

Generate points on Array Cricle

## vArrayPointsOnGrid

Generate points on Array Grid

## vArrayBuffer

FIFO (first in first out) in an array

## vArrayCameraProjection

Transforms Array using a perspective projection matrix

## vArrayInterpolate

Interpolate between two arrays

## vArrayLogicBetween

Logic Between Operations on an Array. If value of array is between min and max then the value is 1 else 0

## vArrayMapRange

Map Range Operations on an Array

## vArrayMath

Math Operations on an Array

## vArrayPacker

Pack Operations on an Array

## vArrayRotateValues

Rotate Array Between Operations on an Array

## vArraySin

Math Sin - Cos Operations on an Array

## vArrayTransform

Transforms an array

## vArrayUnPacker

Unpack Operations on an Array

## vArrayWave

Animates an array

## vArrayCreatePolylineDots

For testing- Create a polygon dot shapes from a ScriptVal based Lua table of XY point pairs

## vArrayCreatePolylineDotsColor

For testing- Create a polygon dot shapes from a ScriptVal based Lua table of XY point pairs

## vArrayRenderWireframes

For testing- Create a polygon wireframe shapes from an array of XY point pairs, and an array of edge index values

## vArrayAccumulator

Temporally concatenate array elements

## vArrayAccumulatorOBJ

Temporally concatenate Wavefront OBJ geometry with edge and point array elements

## vArrayAppend

Append Array to an array

## vArrayAppendGroup

Append Array Groups to an array

## vArrayInfo

Returns info of an array

## vArrayMerge

Dynamically join Array elements into one table

## vArrayMergeOBJ

Merge Wavefront OBJ geometry with edge and point Arrays

## vArraySlice

Trimming the start and end of single or multi-dimensional arrays

## vHTML_UIControl_Color


## vHTML_UIControl_Hero


## vJSONCircularPoints

Distributes points in circular form

## vJSONCreate

Creates an Array based on selected modes

## vJSONCreateJSONFont

Generates a JSON array-based font

## vJSONCreateList

Create an array by entering each value manually

## vJSONCreateMinMax

Creates Array based on Max/Min operations

## vJSONCreateRandom

Creates a random JSON Array

## vJSONCreateTextFont

Creates Text Font

## vJSONFromOBJ

Convert Wavefront OBJ mesh data into a JSON format

## vJSONFromXYZ

Convert ASCII XYZ point cloud data into a JSON format

## vJSONGenerateSphere

Generates a Sphere with longitude/latitude lines

## vJSONLissajouseSpline

Generate a Lissajouse spline

## vJSONMapGeoCoordsSpherical

Converts geographic coordinates (latitude, longitude) into spherical coordinates

## vJSONMapGeoJSON

Map from geographic coordinates

## vJSONMercatorCoord

Center of latitudes and longitudes to a local coordinate system

## vJSONMercatorCoordsArray

Project a Mercator map and center latitude-longitude coordinates to a local system

## vJSONPhyllotaxis

Generate points on Phyllotaxis

## vJSONPointParticles

Simple particle generator

## vJSONPointsHexagonGrid

Generate points on Hexagon Grid

## vJSONPointsOnCircle

Generate points on a cricle

## vJSONPointsOnCube

Generate points on cube

## vJSONPointsOnGrid

Generate point on Array Grid

## vJSONPointsOnRectangle

Generate points on Rectangle

## vJSONPointsOnSphere

Generate points on Sphere

## vJSONLogicBetween

Logic Between Operations on an Array

## vJSONArrayIterator

Loop an array

## vJSONCameraProjection

Transforms arrays using a perspective projection matrix

## vJSONConvert2D-3D

Convert 2D array to 3D array

## vJSONDisplaceValues

Displace XYZ positions using noise with spherical falloff

## vJSONInterpolate

Interpolate between corresponding values in two arrays

## vJSONInterpolateRandom

Interpolate between two randomly chosen values

## vJSONMapRange

Apply range mapping operations to an array

## vJSONMath

Apply mathematical operations to an array

## vJSONPacker

Pack an array into a new array

## vJSONParallelPointsOffSet

Offset the parallel points in an array

## vJSONPerlin3Noise

Perlin Noise function on array values

## vJSONPointsConnect

Connect points in an array

## vJSONPointsOnArc

Generate points on an arc

## vJSONRotateValues

Rotate an array indexes and values

## vJSONSin

Math Sin - Cos Operations on an Array

## vJSONSortByDistance

Sort array by point distance

## vJSONTranslate

Creates a vector from an array

## vJSONUnPacker

Unpack Operations on an Array

## vJSONWave

Animates an Array

## vJSONAppend

Append Array to an array

## vJSONAppendGroup

Append Array Groups to an array

## vJSONBuffer

FIFO (first in first out) in an array

## vJSONInfo

Returns info of an array

## vJSONMerge

Dynamically join JSON elements into one table

## vJSONReducePoints

Reduce points Operations on an Array

## vJSONSlice

Trimming the start and end of single or multi-dimensional arrays

## vNumberSchlickBias

Custom easing curve via schlick bias and gain

## vNumberCircleCoordinates

Calculates a vector (x and y) coordinates of a circle

## vNumberDegreeToVector

Calculates a vector (x and y) based on an angle in degrees

## vNumberSmootherStep

Performs a interpolate smoothly between two input values, ensuring smooth acceleration and deceleration

## vNumberTween

Tween numbers

## vNumberWave

Creates a Delay while passing a Fusion Number object

## vScriptValCreateList

Create an array by entering each value manually

## vScriptValCameraProjection

Transforms ScriptVals using a perspective projection matrix

## vScriptValMath

Math Operations on a ScriptVal

## vScriptValPacker

Pack Operations on a ScriptVal

## vScriptValRotateValues

Rotate ScriptVals Between Operations on a ScriptVal Object

## vScriptValTransform

Transforms a ScriptVal

## vScriptValUnPacker

Unpack Operations on a ScriptVal object

## vScriptValCreatePolylineDots

For testing- Create a polygon dot shapes from a ScriptVal based Lua table of XY point pairs

## vScriptValCreatePolylineDotsColor

For testing- Create a polygon dot shapes from a ScriptVal based Lua table of XY point pairs

## vScriptValRenderWireframes

For testing- Create a polygon wireframe shapes from a ScriptVal based Lua table of XY point pairs, and a ScriptVal Lua table of edge index values

## vScriptValCustom3DShapes

Dynamically create Shape elements
