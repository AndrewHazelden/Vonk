-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayPointParticles"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Simple particle generator",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.pEmitter",
})

function Create()
    -- [[ Creates the user interface. ]]


    InTime = self:AddInput("Time", "timer", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Default = 0,
        LINK_Main = 1,
    })

    InParticles = self:AddInput("Particles Count", "part_count", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 24,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        INP_Integer = true,
    })

    InSizeX = self:AddInput("Size X", "sizex", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0,
    })


    InSizeY = self:AddInput("Size Y", "sizey", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0,
    })

    InSizeZ = self:AddInput("Size Z", "sizez", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0,
    })


    InMovementX = self:AddInput("Movement X", "Movementx", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0.1,
    })


    InMovementY = self:AddInput("Movement Y", "Movementy", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0.1,
    })

    InMovementZ = self:AddInput("Movement Z", "Movementz", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0.1,
    })
    InCenterX = self:AddInput("Center X", "Centerx", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })


    InCenterY = self:AddInput("Center Y", "Centery", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    InCenterZ = self:AddInput("Center Z", "Centerz", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })


    Inlifetime = self:AddInput("lifetime", "lifetime", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0.5,
    })

    InlifetimeMin = self:AddInput("Lifetime Minimum", "lifetimeMin", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.1,
        INP_MaxScale = 1,
        INP_Default = 0.25,

    })

    Inline6Separator = self:AddInput("line6Separator", "line6Separator", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)

end

function Process(req)
    -- [[ Creates the output. ]]


    local timer = InTime:GetValue(req).Value
    local num = InParticles:GetValue(req).Value
    local sizeX = InSizeX:GetValue(req).Value
    local sizeY = InSizeY:GetValue(req).Value
    local sizeZ = InSizeZ:GetValue(req).Value


    local movementX = InMovementX:GetValue(req).Value
    local movementY = InMovementY:GetValue(req).Value
    local movementZ = InMovementZ:GetValue(req).Value

    local centerX = InCenterX:GetValue(req).Value
    local centerY = InCenterY:GetValue(req).Value
    local centerZ = InCenterZ:GetValue(req).Value

    local lifetime = Inlifetime:GetValue(req).Value
    local lifetimeMin = InlifetimeMin:GetValue(req).Value



    local particles = {}
    local positions = {}
    local lifetimes = {}

    local Particle = {}
    Particle.__index = Particle

    function Particle:new()
        local p = setmetatable({}, Particle)
        p.pos = { 0, 0, 0 }
        p.startPos = { 0, 0, 0 }
        p.startTime = 0
        p.lifeTime = 0
        p.lifeTimePercent = 0
        p.endTime = 0
        p.moveVec = { 0, 0, 0 }
        p.isDead = false
        p.random1 = math.random()
        p.random2 = math.random()
        p.random3 = math.random()
        p:reAnimate(0)
        return p
    end

    function Particle:update(time)
        local timeRunning = time - self.startTime
        if time > self.endTime then self.isDead = true end
        self.lifeTimePercent = timeRunning / self.lifeTime

        self.pos[1] = self.startPos[1] + timeRunning * self.moveVec[1]
        self.pos[2] = self.startPos[2] + timeRunning * self.moveVec[2]
        self.pos[3] = self.startPos[3] + timeRunning * self.moveVec[3]
    end

    function Particle:reAnimate(time)
        self.isDead = false
        self.lifeTime = math.random() * (lifetime - lifetimeMin) + lifetimeMin
        if time then
            self.startTime = time
            self.endTime = time + self.lifeTime
        else
            self.startTime = timer - self.lifeTime * math.random()
            self.endTime = timer + self.lifeTime * math.random()
        end

        local r = math.random()

        if centerX then r = r - 0.5 end
        local x = r * sizeX

        r = math.random()
        if centerY then r = r - 0.5 end
        local y = r * sizeY

        r = math.random()
        if centerZ then r = r - 0.5 end
        local z = r * sizeZ

        self.startPos = { x, y, z }
        self.moveVec = {
            math.random() * movementX,
            math.random() * movementY,
            math.random() * movementZ
        }
    end

    local function reset()
        particles = {}

        for i = 1, num do
            local p = Particle:new()
            p:reAnimate()
            table.insert(particles, p)
        end
    end

    local function onTriggered()
        reset()
        local time = timer

        if #positions ~= #particles * 3 then positions = {} end
        if #lifetimes ~= #particles then lifetimes = {} end

        local xyz_table = {} -- Create a structured table to store x, y, z triplets

        for i = 1, #particles do
            if particles[i].isDead then particles[i]:reAnimate(time) end
            particles[i]:update(time)

            local x = particles[i].pos[1]
            local y = particles[i].pos[2]
            local z = particles[i].pos[3]

            table.insert(xyz_table, { x, y, z }) -- Insert structured {x, y, z} table

            lifetimes[i] = particles[i].lifeTimePercent
            if lifetimes[i] > 1.0 then lifetimes[i] = 1.0 end
        end

        return xyz_table
    end

    local result = onTriggered()

    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
