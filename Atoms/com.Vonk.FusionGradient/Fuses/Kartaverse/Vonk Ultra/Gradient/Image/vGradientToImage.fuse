-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientToImage"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "Image",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Image",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create an image from a Fusion Gradient object.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Background",
})

function Create()
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        INPID_InputControl = "GradientControl",
        INP_DoNotifyChanged = true,
        LINK_Main = 1
    })

    self:AddControlPage("Image", {CTID_DIB_ID  = "Icons.Tools.Tabs.Image"})

    InWidth = self:AddInput("Width", "Width", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinScale = 1,
        INP_MaxScale = 8192,
        INP_Default = 1920,
        INP_Integer = true,
    })

    InHeight = self:AddInput("Height", "Height", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinScale = 1,
        INP_MaxScale = 8192,
        INP_Default = 1080,
        INP_Integer = true,
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end

-- =============================================================================
-- main
-- =============================================================================
function NotifyChanged(inp, param, time)
    --[[
    Handles all input control events.

    :param inp: input that triggered a signal
    :type inp: Input

    :param param: parameter object holding the (new) value
    :type param: Parameter

    :param time: current frame number
    :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
    end
end

function Process(req)
    local tbl = {}
    local gradient = InGradient:GetValue(req)

    -- Image Size
    local w = InWidth:GetValue(req).Value
    local h = InHeight:GetValue(req).Value
    local out = Image({
        IMG_Width = w,
        IMG_Height = h,
        IMG_NoData = req:IsPreCalc(),
        IMG_Depth = IMDP_128bitFloat,
    })

    -- Prepare the shape drawing
    local ic = ImageChannel(out, 8)
    local fs = FillStyle() -- Fill Style Object
    local cs = ChannelStyle() -- Channel Style
    local mat = Matrix4() -- Matrix to transform the shapes

    -- Get the number of color entries in the gradient
    totalColors = gradient:GetColorCount() - 1

    -- Auto-size the color bars
    sliceWidth = 1 / totalColors
    -- print("[Slice Width] ", sliceWidth)

    if totalColors <= 1 then
        color = gradient:GetColor(0)
        out:Fill(color)
    else
        for index = 0, totalColors do
            -- Create
            local sh = Shape()
            sh:AddRectangle(-0.1, 0, 0, 0.1, 0.0, 1) -- Left, Right, Top, Bottom, Corner Radius, Precision

            -- Transform
            local cx = index * sliceWidth
            local cy = 0.0 
            mat:Identity()
            mat:Scale(1.0, 10000.0, 1.0)
            mat:Move(cx, cy, 0)

            -- Color fill
            cs.Color = gradient:GetColor(index)
            ic:ShapeFill(sh:TransformOfShape(mat))
            ic:PutToImage("CM_Merge", cs)
        end
    end

    OutImage:Set(req, out)
end
