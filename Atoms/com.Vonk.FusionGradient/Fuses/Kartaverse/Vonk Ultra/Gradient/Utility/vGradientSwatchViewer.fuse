-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientSwatchViewer"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View a Gradient object in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.StickyNote",
})

function math.clamp(val, lower, upper)
    assert(val and lower and upper, "not very useful error message here")
    if lower > upper then lower, upper = upper, lower end -- swap if boundaries supplied the wrong way
    return math.max(lower, math.min(upper, val))
end


function Create()
    -- [[ Creates the user interface. ]]
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    -- Display the image
    InLabel = self:AddInput("", "Label",{
        LINKID_AddBeforeID = "Low",
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_NoLabel = true,
        IC_NoReset = true,
    })

    InLow =  self:AddInput("", "Low", {
         LINKID_AddBeforeID = "Gradient",
            LINKID_DataType = "Number",
            INPID_InputControl = "RangeControl",
            INP_Default = 1,
            INP_MinAllowed = 1,
            INP_MaxScale = 50,
            IC_ControlGroup = 200, 
            IC_ControlID = 0, 
            INP_Integer = true,
           RNGCS_MidName="I<  Range Selection  >I",
            IC_NoLabel = true,
            IC_NoReset = true,
    }) 

    InHigh = self:AddInput("", "High", {
         LINKID_AddBeforeID = "Gradient",
            LINKID_DataType = "Number",
            INPID_InputControl = "RangeControl",
            INP_Default = 50,
            INP_MinAllowed = 1,
            INP_MaxScale = 50,
            IC_ControlGroup = 200,
            IC_ControlID = 1,
            INP_Integer = true,
     })

    InUISeparator0 = self:AddInput("UISeparator0", "UISeparator0", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        INPID_InputControl = "GradientControl",
        LINK_Main = 1,
        IC_Visible = false,
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

--    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "ScrewControl",
--        INP_Integer = true,
--        INP_MaxScale = 100,
--        INP_MinAllowed = 1,
--        INP_Default = 25,
--        LINK_Visible = true,
--        INP_Passive = true,
--        INP_DoNotifyChanged  = true,
--    })

--    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
--        IC_Visible = true,
--        INPID_InputControl = "SeparatorControl",
--        INP_External = false,
--    })

--    InScriptVal = self:AddInput("ScriptVal1", "ScriptVal1", {
--        LINKID_DataType = "ScriptVal",
--        INPID_InputControl = "ScriptValListControl",
--        IC_NoLabel = true,
--        LC_Rows = 24,
--        INP_Passive = true,
--    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
--    if inp == InDisplayLines then
--        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
--        InScriptVal:SetAttrs({LC_Rows = lines})
--        -- Toggle the visibility to refresh the inspector view
--        InScriptVal:SetAttrs({IC_Visible = false})
--        InScriptVal:SetAttrs({IC_Visible = true})
--    elseif inp == InScriptVal then
--        -- Toggle the visibility to refresh the inspector view
--        InScriptVal:SetAttrs({IC_Visible = false})
--        InScriptVal:SetAttrs({IC_Visible = true})
--    end
end

function OnConnected(inp, old, new)
    --if inp == InScriptVal and new ~= nil then
    if new ~= nil then
        -- New connection
    else
        --InScriptVal:SetSource(ScriptValParam({}), 0)
        InLabel:SetSource("", 0)
    end
end

function OnAddToFlow()
    --InScriptVal:SetSource(ScriptValParam({}), 0)
    InLabel:SetSource("", 0)
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = {}
    local gradient = InGradient:GetValue(req)
    local show_dump = InShowDump:GetValue(req).Value
    local start = InLow:GetValue(req).Value
    local stop = InHigh:GetValue(req).Value

    -- Color swatch size
    local tileSize = 32

    -- HTML Header
    local html_str = ""
    html_str = html_str .. [[<html>
<body>
<table style="width:500px; border: 1px solid black;">
<tr style="height:]] .. tostring(tileSize) .. [[px;">]]

    -- Get the number of color entries in the gradient
    --totalColors = gradient:GetColorCount() - 1
    totalColors = stop - start

    -- Process each color swatch
    for index = start, stop, 1 do
    --for index = 0, totalColors, 1 do
        local color = gradient:GetColor(index)
        table.insert(tbl, {P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})

        -- HTML Color
        local r = math.clamp(math.ceil(color.R * 255), 0, 255)
        local g = math.clamp(math.ceil(color.G * 255), 0, 255)
        local b = math.clamp(math.ceil(color.B * 255), 0, 255)
        local rgb_str = string.format("#%02X%02X%02X", r, g, b)
        html_str = html_str .. [[
<td style="background-color:]] .. rgb_str .. [[;"><img width="]] .. tostring(tileSize) .. [[" height="]] .. tostring(tileSize) .. [[" src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAAmRcn2EULJAAAAAnRSTlP/AOW3MEoAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAOSURBVAiZY/j/n4EUBAD9Th/hNOA7twAAAABJRU5ErkJggg=='/></td>
]]
    end

    -- HTML Footer
     html_str = html_str .. "</tr>\n</table>\n</body>\n</html>"

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        dump(html_str)
    end


    --InScriptVal:SetSource(ScriptValParam(tbl), 0)
    InLabel:SetSource(Text(html_str), 0)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end

