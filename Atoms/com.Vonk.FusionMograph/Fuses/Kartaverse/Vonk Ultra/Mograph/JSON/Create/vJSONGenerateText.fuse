-- ============================================================================
-- modules
-- ============================================================================
local utf8 = require("utf8")
local chance = require("chance")
local lorem = require("lorem")
local jsonutils = self and require("vjsonutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vJSONGenerateText"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
	REGID_DataType      = DATATYPE,
	REGID_InputDataType = DATATYPE,
	REGS_Category       = "Kartaverse\\Vonk Ultra\\Mograph\\JSON\\Create",
	REGS_Name           = FUSE_NAME,
	REGS_OpDescription  = "Example, generating random Text and Strings",
	REGS_IconID         = "Icons.Tools.Icons.sRender",
	REG_Version         = 100,
	REG_TimeVariant     = false, -- required to disable caching of the current time parameter
	REGB_Temporal       = true, -- ensures reliability in Resolve 15
	REG_Unpredictable   = true,
})

local Label = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center;"></td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[ A random "everything" Text generator ] </td>
    <td style="text-align: center;"></td>
  </tr>
</table>
]]
function Create()
	InLabel1 = self:AddInput(Label, 'Label1', {
		LINKID_DataType = 'Text',
		INPID_InputControl = 'LabelControl',
		LBLC_MultiLine = true,
		INP_External = false,
		INP_Passive = true,
		IC_NoLabel = true,
		IC_NoReset = true,
	})
	InChanceOp = self:AddInput("Generate Random → ", "chanceOperation", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ComboControl",
		IC_NoReset = true,
		INP_Default = 0.0,
		INP_Integer = true,
		{ CCS_AddString = "None" },
		--Lists
		{ CCS_AddString = "Pick from → input Array" },
		{ CCS_AddString = "Lorem ipsum" },
		--Basics
		{ CCS_AddString = "Bool" },
		{ CCS_AddString = "Character" },
		{ CCS_AddString = "Word" },
		{ CCS_AddString = "Letter" },
		{ CCS_AddString = "Vowel" },
		--Names
		{ CCS_AddString = "Name" },
		{ CCS_AddString = "Male name" },
		{ CCS_AddString = "Male name w/last" },
		{ CCS_AddString = "Name w/last" },
		{ CCS_AddString = "Female name" },
		{ CCS_AddString = "Female name w/last" },
		--Numbers
		{ CCS_AddString = "Hash" },
		{ CCS_AddString = "Integer" },
		{ CCS_AddString = "Integer w/min" },
		{ CCS_AddString = "Integer w/both" },
		--Color
		{ CCS_AddString = "rgb" },
		{ CCS_AddString = "rgba" },
		{ CCS_AddString = "hsl " },
		{ CCS_AddString = "hsla" },
		--Tech
		{ CCS_AddString = "ip" },
		{ CCS_AddString = "ipv4" },
		{ CCS_AddString = "ipv6" },
		--location
		{ CCS_AddString = "Phone" },
		{ CCS_AddString = "Address" },
		{ CCS_AddString = "Street" },
		--Strings
		{ CCS_AddString = "String" },
		{ CCS_AddString = "Syllable" },
		{ CCS_AddString = "Shuffle" },
		INP_DoNotifyChanged = true
	})

	InArrayLength = self:AddInput("Array length", "arrayLength", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = 0,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = 0,
		INP_MaxAllowed = 1e+38,
		INP_Integer = true,
	})
	InSeed = self:AddInput("Seed", "seed", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = -100,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = -1e+38,
		INP_MaxAllowed = 1e+38,
	})

	InEmptySpace2 = self:AddInput('    ', 'EmptySpace2', {
		LINKID_DataType = 'Text',
		INPID_InputControl = 'LabelControl',
		ICD_Width = 0.3,
		INP_External = false,
		INP_Passive = true,
		IC_NoLabel = true,
		IC_NoReset = true,
	})
	Inline1Separator = self:AddInput("line1Separator", "line1Separator", {
		INPID_InputControl = "SeparatorControl",
		IC_Visible = true,
		INP_External = false,
	})
	InSelectMode = self:AddInput("Select Mode → ", "selectMode", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ComboControl",
		IC_NoReset = true,
		INP_Default = 0.0,
		INP_Integer = true,
		{ CCS_AddString = "none" },
		{ CCS_AddString = "Characters" },
		{ CCS_AddString = "Words" },
		{ CCS_AddString = "Sentences" },
		{ CCS_AddString = "Paragraphs" },
	})
	InCharacterCount = self:AddInput("Character Count", "CharacterCount", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = 0,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = 0,
		INP_MaxAllowed = 1e+38,
		INP_Integer = true,
		--LINK_Main = 1,
		LINK_Visible = false,
	})
	InWordCount = self:AddInput("Word Count", "WordCount", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = 0,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = 0,
		INP_MaxAllowed = 1e+38,
		INP_Integer = true,
		--LINK_Main = 1,
		LINK_Visible = false,
	})

	InSentenceCount = self:AddInput("Sentence Count", "sentenceCount", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = 0,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = 0,
		INP_MaxAllowed = 1e+38,
		INP_Integer = true,
		--LINK_Main = 1,
		LINK_Visible = false,
	})

	InParagraphCount = self:AddInput("Paragraph Count", "paragraphCount", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_MinScale = 0,
		INP_MaxScale = 100,
		INP_Default = 0,
		INP_MinAllowed = 0,
		INP_MaxAllowed = 1e+38,
		INP_Integer = true,
		--LINK_Main = 1,
		LINK_Visible = false,
	})
	Inline2Separator = self:AddInput("line2Separator", "line2Separator", {
		INPID_InputControl = "SeparatorControl",
		IC_Visible = true,
		INP_External = false,
	})
	InCase = self:AddInput("Letter Case", "Case", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		{ MBTNC_AddButton = "NONE",  MBTNCD_ButtonWidth = 1 / 3, },
		{ MBTNC_AddButton = "UPPER", MBTNCD_ButtonWidth = 1 / 3, },
		{ MBTNC_AddButton = "LOWER", MBTNCD_ButtonWidth = 1 / 3, },
		INP_Integer = true,
	})

	InShowInput = self:AddInput("Show Input", "ShowInput", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Integer = true,
		INP_Default = 1.0,
		INP_External = false,
		INP_DoNotifyChanged = true
	})
	InScriptValText = self:AddInput("ScriptVal Text", "ScriptValtext", {
		LINKID_DataType = "ScriptVal",
		LINK_Main = 2,
	})

	OutData = self:AddOutput("OutputValData", "OutputValData", {
		LINKID_DataType = DATATYPE,
		LINK_Main = 1
	})
end

function NotifyChanged(inp, param, time)
	local visible
	if inp == InShowInput then
		if param.Value == 1.0 then visible = true else visible = false end
	end

	if inp == InChanceOp then
		if param.Value == 2.0 then
			Inline1Separator:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InSelectMode:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InCharacterCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InWordCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InSentenceCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InParagraphCount:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InArrayLength:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InSeed:SetAttrs({ IC_Visible = false, PC_Visible = false })
		else
			InArrayLength:SetAttrs({ IC_Visible = true, PC_Visible = true })
			InSeed:SetAttrs({ IC_Visible = true, PC_Visible = true })
			Inline1Separator:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InWordCount:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InSelectMode:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InCharacterCount:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InSentenceCount:SetAttrs({ IC_Visible = false, PC_Visible = false })
			InParagraphCount:SetAttrs({ IC_Visible = false, PC_Visible = false })
		end
	end
end

function Process(req)
	local text            = InScriptValText:GetValue(req):GetValue() or {}
	local arrLength       = InArrayLength:GetValue(req).Value
	local result          = {}
	local seed            = InSeed:GetValue(req).Value
	local mode            = InChanceOp:GetValue(req).Value
	local outputData      = {}
	local word_count      = InWordCount:GetValue(req).Value
	local character_count = InCharacterCount:GetValue(req).Value
	local paragraph_count = InParagraphCount:GetValue(req).Value
	local sentence_count  = InSentenceCount:GetValue(req).Value



	-- Seed the random number generator
	chance:seed(seed)
	-- Helper function to capitalize the first letter
	local function firstToUpper(str)
		return str:sub(1, 1):upper() .. str:sub(2)
	end

	local function generateLoremIpsum(mode)
		local txtTbl = {}
		if mode == 0 then
			txtTbl = {}
		elseif mode == 1 then
			for i = 1, character_count do
				table.insert(txtTbl, i, lorem:characters(i))
			end
		elseif mode == 2 then
			for i = 1, word_count do
				table.insert(txtTbl, i, lorem:words(i))
			end
		elseif mode == 3 then
			for i = 1, sentence_count do
				table.insert(txtTbl, i, lorem:sentences(i))
			end
		elseif mode == 4 then
			for i = 1, paragraph_count do
				table.insert(txtTbl, i, lorem:paragraphs(i))
			end
		end

		return txtTbl
	end

	-- Generate random values based on the selected mode
	local function generateRandomValue(mode)
		if mode == 0 then
			return nil
		elseif mode == 1 then
			return tostring(chance:pick(text))
		elseif mode == 2 then
			-- lorem ipsum
			result = generateLoremIpsum(InSelectMode:GetValue(req).Value)
		elseif mode == 3 then
			return firstToUpper(tostring(chance:bool()))
		elseif mode == 4 then
			return tostring(chance:character())
		elseif mode == 5 then
			return tostring(firstToUpper(chance:word()))
		elseif mode == 6 then
			return tostring(chance:letter())
		elseif mode == 7 then
			return tostring(chance:vowel())
		elseif mode == 8 then
			return tostring(chance:name())
		elseif mode == 9 then
			return tostring(chance:name(true))
		elseif mode == 10 then
			return tostring(chance:male())
		elseif mode == 11 then
			return tostring(chance:male(true))
		elseif mode == 12 then
			return tostring(chance:female())
		elseif mode == 13 then
			return tostring(chance:female(true))
		elseif mode == 14 then
			return tostring(chance:hash())
		elseif mode == 15 then
			return tostring(chance:integer())
		elseif mode == 16 then
			return tostring(chance:integer(500))
		elseif mode == 17 then
			return tostring(chance:integer(-500, 500))
		elseif mode == 18 then
			local r, g, b = chance:rgb()
			return tostring(r .. "," .. g .. "," .. b)
		elseif mode == 19 then
			local r, g, b, a = chance:rgba()
			return tostring(r .. "," .. g .. "," .. b .. "," .. a)
		elseif mode == 20 then
			local h, s, l = chance:hsl()
			return tostring(h .. "," .. s .. "," .. l)
		elseif mode == 21 then
			local h, s, l, a = chance:hsla()
			return tostring(h .. "," .. s .. "," .. l .. "," .. a)
		elseif mode == 22 then
			return tostring(chance:ip())
		elseif mode == 23 then
			return tostring(chance:ipv4())
		elseif mode == 24 then
			return tostring(chance:ipv6())
		elseif mode == 25 then
			return tostring(chance:phone())
		elseif mode == 26 then
			return tostring(chance:address())
		elseif mode == 27 then
			return tostring(chance:street())
		elseif mode == 28 then
			return tostring(firstToUpper(chance:string(math.random(3, 10))))
		elseif mode == 29 then
			return tostring(chance:syllable())
		elseif mode == 30 then
			return tostring(table.unpack(chance:shuffle({ "A", "B", "C", "D", "E", "F", "GH", "", "I", "J", "K", "L", "M",
				"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h",
				"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2",
				"3", "4", "5", "6", "7", "8", "9"
			})))
		end
	end

	if mode == 0 then
		--clear the table
		for k, _ in pairs(result) do result[k] = nil end
	else
		-- Generate random values based on the selected mode
		for i = 1, arrLength do
			table.insert(result, i, generateRandomValue(mode))
		end
	end

	-- Upper and Lowercase
	local case = InCase:GetValue(req).Value
	local modified_text


	for i = 1, #result do
		modified_text = result[i]
		if case == 0 then
			modified_text = result[i]
		elseif case == 1 then
			modified_text = utf8.upper(result[i])
		elseif case == 2 then
			modified_text = utf8.lower(result[i])
		end

		table.insert(outputData, i, modified_text)
	end


	local json_str_out = jsonutils.encode(outputData)
	OutData:Set(req, Text(json_str_out))
end
