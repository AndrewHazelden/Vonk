-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointTimeSpeed"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Temporal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Time based operations on a Fusion Point object.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
    REGS_IconID = "Icons.Tools.Icons.TimeSpeed",
})

function Create()
    -- [[ Creates the user interface. ]]
    InPoint = self:AddInput("Point", "Point", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
        INP_SendRequest = false,
        INP_Required = false,
        LINK_Main = 1,
    })

    InSpeed = self:AddInput("Speed" , "Speed" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -100.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 2
    })

    InDelay = self:AddInput("Delay" , "Delay" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -100.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 3
    })
    
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutPoint = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InSpeed:SetAttrs({LINK_Visible = visible})
        InDelay:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local t_current = self.Comp.CurrentTime
    local t_globalstart = self.Comp.GlobalStart
    local t_speed = InSpeed:GetValue(req).Value
    local t_delay = InDelay:GetValue(req).Value

    local t = ( ( ( t_current - t_delay ) - t_globalstart ) * t_speed ) + t_globalstart
    local out = InPoint:GetSource(t, req:GetFlags())

    OutPoint:Set(req, out)
end
