-- Todo: Remove the XML <materialx> headers automatically from the input text streams

-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMTLXMerge"
DATATYPE = "Text"
MAX_INPUTS = 64

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\MTLX\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Dynamically joins separate MTLX content into one.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.MultiMerge",
})

function Create()
    -- [[ Creates the user interface. ]]
    self:RemoveControlPage("Controls")
    self:AddControlPage("Material", {CTID_DIB_ID  = "Icons.Tools.Tabs.Material"})
    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = MAX_INPUTS,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })
    InExportHeaders = self:AddInput("Export XML Headers", "ExportHeaders", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_Passive = false,
        INP_DoNotifyChanged = true
    })
    InXML1 = self:AddInput("XML1", "XML1", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
        INP_External = true,
        INP_Required = false
    })
    -- InSeparator = self:AddInput("Separator", "Separator", {
        -- LINKID_DataType = "Text",
        -- INPID_InputControl = "TextEditControl",
        -- TEC_Lines = 1
    -- })
    OutXML = self:AddOutput("OutputXML", "OutputXML", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function OnAddToFlow()
    --[[ Callback triggered when adding the Fuse to the flow view. ]]
    -- find highest existing input
    local highest_input = 0

    for i = 1, MAX_INPUTS do
        local input = self:FindInput("XML" .. tostring(i))

        if input == nil then
            break
        end

        highest_input = i
    end

    -- add inputs
    -- NOTE: start at 2, inputs 1 always exists
    for i = 2, highest_input do
        self:AddInput("XML" .. i, "XML" .. i, {
            LINKID_DataType = "Text",
            LINK_Main = i,
            INP_Required = false,
            INP_External = true,
            INP_DoNotifyChanged = true
        })
    end

    -- set slider maximum
    InWhich:SetAttrs({INP_MaxScale = highest_input, INP_MaxAllowed = highest_input})
end

function OnConnected(inp, old, new)
    --[[ Callback triggered when connecting a Fuse to the input of this Fuse. ]]
    local inp_nr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "XML(%d+)"))
    local max_nr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    if inp_nr then
        -- add input if maximum inputs is not exceeded and connection is not empty
        if inp_nr >= max_nr and max_nr < MAX_INPUTS and new ~= nil then
            -- set slider maximum
            InWhich:SetAttrs({INP_MaxScale = inp_nr, INP_MaxAllowed = inp_nr})

            -- add extra input
            self:AddInput("XML" .. (inp_nr + 1), "XML" .. (inp_nr + 1), {
                LINKID_DataType = "Text",
                LINK_Main = (inp_nr + 1),
                INP_Required = false,
                INP_External = true,
                INP_DoNotifyChanged = true
            })
        end
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local exportHeaders = InExportHeaders:GetValue(req).Value

    local input_count = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    local seperator = "\n"
    local values = {}

    for i = 1, input_count do
        -- get input from index
        local input = self:FindInput("XML" .. tostring(i))

        -- get text from input
        if input ~= nil and input:GetSource(req.Time, req:GetFlags()) then
            local inp_text = input:GetSource(req.Time, req:GetFlags()).Value

            if inp_text == nil then
                inp_text = ""
            end

            table.insert(values, inp_text)
        end
    end

    local text = ""

    for key, value in pairs(values) do
        if key == 1 then
            text = value
        else
            text = text .. seperator .. value
        end
    end

    -- Append the XML header data
    local xml_str = ""
    if exportHeaders == 1 then 
        xml_str = xml_str .. [[
<?xml version="1.0"?>
<materialx version="1.38" colorspace="lin_rec709">
]]
    end

    xml_str = xml_str .. text .. seperator

    if exportHeaders == 1 then 
        xml_str = xml_str .. [[
</materialx>
]]
    end

    local out = Text(xml_str)
    OutXML:Set(req, out)
end
