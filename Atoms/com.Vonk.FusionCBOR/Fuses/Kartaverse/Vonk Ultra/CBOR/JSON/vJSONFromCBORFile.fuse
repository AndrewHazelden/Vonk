-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
local cbor = self and require("cbor") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vJSONFromCBORFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\CBOR\\JSON",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Casts a CBOR binary file into JSON text.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "Any Filetype (*.*)|*.*|",
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutJSON = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local fp = assert(io.open(abs_path , "rb"))
    local tbl = cbor.decode_file(fp)
    fp:close()

    local sort = InSort:GetValue(req).Value

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    -- local json_str = jsonutils.encode(tbl)
    local json_str = jsonutils.encode_indent(tbl, true)
    local out = Text(json_str)

    OutJSON:Set(req, out)
end
