-- ============================================================================
-- modules
-- ============================================================================
local matrix = self and require("matrix") or nil
local matrixutils = self and require("vmatrixutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMatrixToMOVE"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Matrix\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Export translation matrix content to the Maya MOVE ASCII format",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.Transform3D",
})

function Create()
    -- [[ Creates the user interface. ]]
local ConnectionLabel = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center; padding: 5px;">[vMatrixCreateTRS] →</td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[vMatrixToMOVE]</td>
    <td style="text-align: center; padding: 5px;">→ [vTextToFile]</td>
  </tr>
</table>
]]

    InConnectionLabel = self:AddInput(ConnectionLabel, "ConnectionLabel", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_ControlPage = -1,
        IC_NoLabel = true,
        IC_NoReset = true,
    })

    InChannelsNest = self:BeginControlNest("Channels", "ChannelsNest", true, {})
        InExportTranslation = self:AddInput("Export Translation", "ExportTranslation", {
           LINKID_DataType = "Number",
           INPID_InputControl = "CheckboxControl",
           INP_Integer = true,
           INP_Default = 1.0,
           INP_External = false,
           INP_Passive = false,
           INP_DoNotifyChanged = true
        })
    
        InExportRotation = self:AddInput("Export Rotation ", "ExportRotation ", {
           LINKID_DataType = "Number",
           INPID_InputControl = "CheckboxControl",
           INP_Integer = true,
           INP_Default = 1.0,
           INP_External = false,
           INP_Passive = false,
           INP_DoNotifyChanged = true
        })
    
        InExportScale = self:AddInput("Export Scale ", "ExportScale", {
           LINKID_DataType = "Number",
           INPID_InputControl = "CheckboxControl",
           INP_Integer = true,
           INP_Default = 0.0,
           INP_External = false,
           INP_Passive = false,
           INP_DoNotifyChanged = true
        })
    self:EndControlNest()

    InTimeNest = self:BeginControlNest("Time", "TimeNest", true, {})
        InStartFrame = self:AddInput("Start Frame", "StartFrame", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_MinAllowed = 0,
            INP_MaxAllowed = 1e+38,
            INP_MaxScale = 2000,
            INP_Integer = true,
            INP_Default = 0,
            IC_Steps = 1.0,
            IC_Visible = true,
            LINK_Main = 3,
        })
    
        InEndFrame = self:AddInput("End Frame", "EndFrame", {
            LINKID_DataType = "Number",
            INPID_InputControl = "ScrewControl",
            INP_MinAllowed = 0,
            INP_MaxAllowed = 1e+38,
            INP_MaxScale = 2000,
            INP_Integer = true,
            INP_Default = 0,
            IC_Steps = 1.0,
            IC_Visible = true,
            LINK_Main = 4,
        })
    
        InStep = self:AddInput("Step", "Step", {
            LINKID_DataType = "Number",
            INPID_InputControl = "SliderControl",
            TEC_Lines = 1,
            INP_MinScale = -120,
            INP_MaxScale = 120,
            INP_Default = 1,
            INP_MinAllowed = 1,
            INP_MaxAllowed = 1e+38,
            LINK_Main = 5,
        })
    self:EndControlNest()

    InTranspose = self:AddInput("Transpose Matrix", "Transpose", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        IC_Visible = false,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InMatrix1 = self:AddInput("Matrix1", "Matrix1", {
        LINKID_DataType = "Text",
        --INPID_InputControl = "TextEditControl",
        --TEC_Wrap = true,
        LINK_Main = 1
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InStartFrame:SetAttrs({LINK_Visible = visible})
        InEndFrame:SetAttrs({LINK_Visible = visible})
        InStep:SetAttrs({LINK_Visible = visible})
    end
end

function OnConnected(inp, old, new)
end

function MatrixConvert(mx_1_str, transpose, exportTranslation, exportRotation, exportScale)
    -- Transpose matrix
    local mx_orig = matrixutils.matrix_from_string(mx_1_str)
    local mx = mx_orig
    if transpose == 1 then
        mx = matrix.transpose(mx_orig)
    end

    -- Translate
    local tx = mx[4][1]
    local ty = mx[4][2]
    local tz = mx[4][3]

    -- Rotate
    local rx = 0
    local ry = 0
    local rz = 0

    if math.cos(-math.asin(mx[1][3])) ~= 0 then
        rx = math.deg(math.atan2(mx[2][3], mx[3][3]))
    else
        rx = math.deg(math.atan2(mx[2][1], mx[2][2]))
    end

    ry = math.deg(-math.asin(mx[1][3]))

    if math.cos(-math.asin(mx[1][3])) ~= 0 then
        rz = math.deg(math.atan2(mx[1][2], mx[1][1]))
    end

    -- Scale
    local sx = math.sqrt(mx[1][1]^2 + mx[1][2]^2 + mx[1][3]^2)
    local sy = math.sqrt(mx[2][1]^2 + mx[2][2]^2 + mx[2][3]^2)
    local sz = math.sqrt(mx[3][1]^2 + mx[3][2]^2 + mx[3][3]^2)

    local tbl = {
        Translate = {
            X = tx,
            Y = ty,
            Z = tz,
        },
        Rotate = {
            X = rx,
            Y = ry,
            Z = rz,
        },
        Scale = {
            X = sx,
            Y = sy,
            Z = sz,
        },
    }

    local t_str = ""
    if exportTranslation == 1 then
        t_str = string.format("%.4f %.4f %.4f ", tx, ty, tz)
    end
    
    local r_str = ""
    if exportRotation == 1 then
        r_str = string.format("%.4f %.4f %.4f ", rx, ry, rz)
    end
    
    local s_str = ""
    if exportScale == 1 then
        s_str = string.format("%.4f %.4f %.4f ", sx, sy, sz)
    end

    local move_str = t_str .. r_str .. s_str .. "\n"
    return move_str
end

function Process(req)
    -- [[ Creates the output. ]]
    local exportTranslation = InExportTranslation:GetValue(req).Value
    local exportRotation = InExportRotation:GetValue(req).Value
    local exportScale = InExportScale:GetValue(req).Value

    local start_frame = tonumber(InStartFrame:GetValue(req).Value)
    local end_frame = tonumber(InEndFrame:GetValue(req).Value)
    local step = tonumber(InStep:GetValue(req).Value)

    local transpose = InTranspose:GetValue(req).Value
    local move_str = ""

    for i = start_frame, end_frame, step do
        if InMatrix1:GetSource(i, req:GetFlags()) and InMatrix1:GetSource(i, req:GetFlags()).Value then
            local mx_1_str = InMatrix1:GetSource(i, req:GetFlags()).Value
            if mx_1_str ~= nil and mx_1_str ~= "" then
                move_str = move_str .. MatrixConvert(mx_1_str, transpose, exportTranslation, exportRotation, exportScale)
            end
        end

        -- Update the rendering progress bar on the node
        local progress = i/end_frame
        self:SetProgress(progress)
    end

    local out = Text(move_str)
    OutText:Set(req, out)
end
