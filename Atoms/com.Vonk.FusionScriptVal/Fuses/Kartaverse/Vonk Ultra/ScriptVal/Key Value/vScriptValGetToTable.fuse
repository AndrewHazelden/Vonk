-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValGetToTable"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Key Value",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Gets the value of a ScriptVal key as a table.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    GetKey = [[
comp:StartUndo("Rename Node")

tool = tool or comp.ActiveTool
NewName = "scriptval_" .. tostring(tool:GetInput("Key"))
tool:SetAttrs({TOOLS_Name = NewName})

comp:EndUndo()
]]

    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InKey = self:AddInput("Key", "Key", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    GetKeyBtn = self:AddInput("Get Key", "GetKeyBtn", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetKey,
        ICD_Width = 1.0,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InKey:SetAttrs({LINK_Visible = true})
        else
            InKey:SetAttrs({LINK_Visible = false})
        end
    end
end

function get(t, key)
    --[[
        Returns the value of a key in a table.

        :param t: Table to get key value for.
        :type t: table

        :param key: Key to get value of.
        :type key: string

        :rtype: ?
    ]]
    local value = nil
    local found = false

    for k, v in pairs(t) do
        if k == key then
            value = v
            found = true
            break
        end
    end

    if not found then
        error(string.format("no key '%s' found in ScriptVal table", key))
    end

    return value
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local key = InKey:GetValue(req).Value

    local value = nil

    if key ~= nil then
        value = get(tbl or {}, key)
    end

    OutScriptVal:Set(req, ScriptValParam(value))
end
