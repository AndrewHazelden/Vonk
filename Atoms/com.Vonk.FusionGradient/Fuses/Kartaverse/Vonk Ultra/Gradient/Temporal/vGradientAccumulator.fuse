-- ============================================================================
-- modules
-- ============================================================================
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vGradientAccumulator"
DATATYPE = "Gradient"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Gradient\\Temporal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Temporally concatenate Gradients.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TimeSpeed",
})

function Create()
    -- [[ Creates the user interface. ]]
    self:RemoveControlPage("Controls")
    self:AddControlPage("Color", {CTID_DIB_ID  = "Icons.Tools.Tabs.Color"})

    InGradient = self:AddInput("Gradient", "Gradient", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })

    InStartFrame = self:AddInput("Start Frame", "StartFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1e+38,
        INP_MaxScale = 2000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = true,
        LINK_Main = 2,
    })

    InEndFrame = self:AddInput("End Frame", "EndFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1e+38,
        INP_MaxScale = 2000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = true,
        LINK_Main = 3,
    })

    InStep = self:AddInput("Step", "Step", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -120,
        INP_MaxScale = 120,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        LINK_Main = 4,
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutGradient = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Gradient",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InStartFrame:SetAttrs({LINK_Visible = visible})
        InEndFrame:SetAttrs({LINK_Visible = visible})
        InStep:SetAttrs({LINK_Visible = visible})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local start_frame = tonumber(InStartFrame:GetValue(req).Value)
    local end_frame = tonumber(InEndFrame:GetValue(req).Value)
    local step = tonumber(InStep:GetValue(req).Value)
    local show_dump = InShowDump:GetValue(req).Value

    local tbl = {}

    for i = start_frame, end_frame, step do
        local gradient = InGradient:GetSource(i, req:GetFlags())
        if gradient ~= nil then
            -- Get the number of color entries in the gradient
            local totalColors = gradient:GetColorCount() - 1
            for index = 0, totalColors, 1 do
                local color = gradient:GetColor(index)
                table.insert(tbl, {P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
                -- dump({P = tonumber(index / totalColors), R = tonumber(color.R), G = tonumber(color.G), B = tonumber(color.B), A = tonumber(color.A)})
            end
        end

        -- Update the rendering progress bar on the node
        local progress = i/end_frame
        self:SetProgress(progress)
    end

   if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    local out_gradient = Gradient()
    -- Generate the new gradient output
    local outTotalColors = #tbl - 1
    for key, value in ipairs(tbl) do
        out_gradient:AddColor(((key - 1) / outTotalColors), Pixel({R = tonumber(value.R), G = tonumber(value.G), B = tonumber(value.B), A = tonumber(value.A)}))
    end

    OutGradient:Set(req, out_gradient)
end
