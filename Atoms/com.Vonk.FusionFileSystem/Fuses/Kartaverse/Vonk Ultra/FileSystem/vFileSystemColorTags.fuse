-- ============================================================================
-- modules
-- ============================================================================
local filesystemutils = self and require("vfilesystemutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vFileSystemColorTags"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\FileSystem",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Apply macOS Finder Color tags.",
    REGS_OpIconString = FUSE_NAME,

    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,

    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        -- INPID_InputControl = "TextEditControl",
        INPID_InputControl = "FileControl",
        FC_PathBrowse = false,
        INP_Passive = true,
        TEC_Lines = 1,
        LINK_Main = 1
    })

    InColor = self:AddInput("Color", "Color", {
        INPID_InputControl = "ComboControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        ICD_Width = 1,
        CC_LabelPosition = "Horizontal",
        {CCS_AddString = "No Color"},
        {CCS_AddString = "Orange"},
        {CCS_AddString = "Red"},
        {CCS_AddString = "Yellow"},
        {CCS_AddString = "Blue"},
        {CCS_AddString = "Purple"},
        {CCS_AddString = "Green"},
        {CCS_AddString = "Gray"},
        INP_Default = 0,
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function Process(req)
    -- [[ Creates the output. ]]
    local color = InColor:GetValue(req).Value

    local path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(path)

    if jit.os == "OSX" then
        local command = [[/usr/bin/osascript -e 'tell application "Finder" to set label index of alias POSIX file "]] .. abs_path .. [[" to ]] .. color .. [[']]
        -- print('[Launch Command] ', command)
        os.execute(command)
    end

    OutText:Set(req, Text(path))
end
