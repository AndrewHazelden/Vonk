-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValTranslate"
DATATYPE = "ScriptVal"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\ScriptVal\\Modify",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Transforms a array positions.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})
local ConnectionLabel = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center;">[ vScriptValUnPacker ]  → </td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[ ]] ..
    FUSE_NAME .. [[ ]</td>
    <td style="text-align: center;">  → [ vScriptValPacker ]</td>
  </tr>
</table>
]]
function Create()
    -- [[ Creates the user interface. ]]
    InLabelConnect = self:AddInput(ConnectionLabel, 'LabelConnect', {
        LINKID_DataType = 'Text',
        INPID_InputControl = 'LabelControl',
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_ControlPage = -1,
        IC_NoLabel = true,
        IC_NoReset = true,
    })

    self:BeginControlNest("Translation", "Translation", true, { LBLC_PickButton = false })

    TranslateX = self:AddInput("X Offset", "Translate_X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        LINK_Main = 2
    })

    TranslateY = self:AddInput("Y Offset", "Translate_Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        LINK_Main = 3
    })

    TranslateZ = self:AddInput("Z Offset", "Translate_Z", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        LINK_Main = 4,
    })
    self:EndControlNest()


    self:BeginControlNest("Scale", "Scale", true, { LBLC_PickButton = false })
    InLockScale = self:AddInput("Lock X/Y", "LockXY", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1,
        INP_DoNotifyChanged = true,
    })

    ScaleX = self:AddInput("X Scale", "Scale.X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1.0,
    })

    ScaleY = self:AddInput("Y Scale", "Scale.Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1.0,
        IC_Visible = false,
        PC_Visible = false
    })
    self:EndControlNest()


    InArray = self:AddInput("Array A", "ArrayA", {
        INPID_InputControl = "ImageControl",
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 1,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        TranslateX:SetAttrs({ LINK_Visible = visible })
        TranslateY:SetAttrs({ LINK_Visible = visible })
        TranslateZ:SetAttrs({ LINK_Visible = visible })

        ScaleX:SetAttrs({ LINK_Visible = visible })
        ScaleY:SetAttrs({ LINK_Visible = visible })
    end

    if inp == InLockScale then
        if param.Value > 0.5 then
            ScaleX:SetAttrs({ LINKS_Name = "Scale" })
            ScaleY:SetAttrs({ IC_Visible = false, PC_Visible = false })
        else
            ScaleX:SetAttrs({ LINKS_Name = "X Scale" })
            ScaleY:SetAttrs({ IC_Visible = true, PC_Visible = true })
        end
    end
end

function Process(req)
    local inArr = InArray:GetValue(req):GetValue() or {}

    local transX = TranslateX:GetValue(req).Value
    local transY = TranslateY:GetValue(req).Value
    local transZ = TranslateZ:GetValue(req).Value
    local scaleX = ScaleX:GetValue(req).Value
    local scaleY = ScaleY:GetValue(req).Value
    local scaleZ = ScaleX:GetValue(req).Value

    local resultArr = {}

    local function doTransform()
        local arr = inArr

        resultArr = {}
        local scx = scaleX
        local scy = scaleY
        local scz = scaleZ
        local transx = transX
        local transy = transY
        local transz = transZ

        for i = 1, #arr, 3 do
            resultArr[i] = arr[i] + transx
            resultArr[i + 1] = arr[i + 1] + transy
            resultArr[i + 2] = arr[i + 2] + transz

            resultArr[i] = resultArr[i] * scx
            resultArr[i + 1] = resultArr[i + 1] * scy
            resultArr[i + 2] = resultArr[i + 2] * scz
        end
        return resultArr
    end

    local result = doTransform()

    OutScriptVal:Set(req, ScriptValParam(result))
end
