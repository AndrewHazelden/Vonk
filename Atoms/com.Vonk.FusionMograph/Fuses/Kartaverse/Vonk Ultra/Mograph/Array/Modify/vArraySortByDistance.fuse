-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArraySortByDistance"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Modify",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Sort array by point distance",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})
local ConnectionLabel = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center;">[ vArrayUnPacker ]  → </td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[ ]] ..
    FUSE_NAME .. [[ ]</td>
    <td style="text-align: center;">  → [ vArrayPacker ]</td>
  </tr>
</table>
]]
function Create()
    -- [[ Creates the user interface. ]]
    InLabelConnect = self:AddInput(ConnectionLabel, 'LabelConnect', {
        LINKID_DataType = 'Text',
        INPID_InputControl = 'LabelControl',
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_ControlPage = -1,
        IC_NoLabel = true,
        IC_NoReset = true,
    })
    InSmallDist = self:AddInput("Smallest Distance", "smallDist", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 10.0,
        INP_Default = 0.01,
        INP_MinAllowed = 0.0,
        INP_MaxAllowed = 1e+38,

    })
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InArray = self:AddInput("Array", "Array", {
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 1,

    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
end

function Process(req)
    local arrayA = jsonutils.decode(InArray:GetValue(req).Value).array or {}
    local smallestDist = InSmallDist:GetValue(req).Value

    -- Optimized distance function
    local function dist(x1, y1, z1, x2, y2, z2)
        local xd, yd, zd = x2 - x1, y2 - y1, z2 - z1
        return math.sqrt(xd * xd + yd * yd + zd * zd)
    end

    if not arrayA then
        print("Connect Array")
        return
    end

    -- Convert flat array to structured list
    local data = {}
    for i = 1, #arrayA, 3 do
        data[#data + 1] = {
            x = arrayA[i],
            y = arrayA[i + 1],
            z = arrayA[i + 2],
            found = false,
            pos = 0,
            idx = (i - 1) / 3
        }
    end

    -- Start sorting with nearest-neighbor approach
    local lastPoint = data[1]
    lastPoint.found = true

    for i = 2, #data do -- Start from 2nd since the 1st is fixed
        local smallestIndex = nil
        local smallDist = smallestDist

        for j = 2, #data do -- No need to check the first (already set)
            if not data[j].found then
                local d = dist(lastPoint.x, lastPoint.y, lastPoint.z, data[j].x, data[j].y, data[j].z)
                if d < smallDist then
                    smallDist = d
                    smallestIndex = j
                end
            end
        end

        if smallestIndex then
            data[smallestIndex].pos = i
            data[smallestIndex].found = true
            lastPoint = data[smallestIndex]
        end
    end

    -- Sort by position
    table.sort(data, function(a, b) return a.pos < b.pos end)

    -- Flatten sorted data back to array
    local result = {}
    for i = 1, #data do
        local idx = (i - 1) * 3
        result[idx + 1] = data[i].x
        result[idx + 2] = data[i].y
        result[idx + 3] = data[i].z
    end

    -- Encode JSON output
    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
