-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayLissajouseSpline"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Generate a Lissajouse spline.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]

    InFormula = self:AddInput("Formula", "formula", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0.0,
        { MBTNC_AddButton = "Formula A", MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "Formula B", MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "Formula C", MBTNCD_ButtonWidth = 1 / 3, },
        INP_Integer = true,
        INP_DoNotifyChanged = true,
    })

    InNumA = self:AddInput("Number A", "num_a", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 6,
        INP_Default = 5,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 6,
        INP_Integer = true,
    })

    InNumB = self:AddInput("Number B", "num_b", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 6,
        INP_Default = 4,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 6,
        INP_Integer = true,
    })

    InNumC = self:AddInput("Number C", "num_c", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 10,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 10,
        INP_Integer = true,
    })

    InNumD = self:AddInput("Number D", "num_d", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 6,
        INP_Default = 2,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 6,
        INP_Integer = true,
    })

    InNumPoints = self:AddInput("Points Count", "numPoints", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 10000,
        INP_Default = 100,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        INP_Integer = true,
    })

    InStep = self:AddInput("Step", "step", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Default = 40,
        INP_MinScale = 1,
        INP_MaxScale = 300,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        INP_Integer = true,
    })

    InScale = self:AddInput("Scale Factor", "scaleFactor", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0,
        INP_MaxScale = 1,
        INP_Default = 1,
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1,
    })

    Inline6Separator = self:AddInput("line6Separator", "line6Separator", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)

end

function Process(req)
    -- [[ Creates the output. ]]

    local inForm = InFormula:GetValue(req).Value
    local inA = InNumA:GetValue(req).Value
    local inB = InNumB:GetValue(req).Value
    local inC = InNumC:GetValue(req).Value
    local inD = InNumD:GetValue(req).Value
    local result = {}

    local scaleFactor = InScale:GetValue(req).Value

    local function calc()
        local numPoints = InNumPoints:GetValue(req).Value
        local step = InStep:GetValue(req).Value

        local arr = {}
        local x, y, z = 0, 0, 0
        local form = inForm
        local th = 0.02

        for i = 0, numPoints - step, step do
            local index = i / step

            if form == 0 then
                x = math.sin((i * inA) * 0.001)
                y = math.cos((i * inB) * 0.001)
                z = math.sin((i * inC) * 0.001)
            elseif form == 1 then
                x = (math.cos((i * inA) * 0.001) + math.cos((i * inB) * 0.001)) / 2
                y = (math.sin((i * inA) * 0.001) + math.sin((i * inC) * 0.001)) / 2
                z = math.sin((i * inD) * 0.001)
            elseif form == 2 then
                x = (math.sin((i * inA) * 0.001) * (1 + math.cos((i * inB) * 0.001))) / 2
                y = (math.sin((i * inA) * 0.001) * (1 + math.sin((i * inC) * 0.001))) / 2
                z = math.sin((i * inD) * 0.001)
            end

            -- Apply scaling
            x = x * scaleFactor
            y = y * scaleFactor
            z = z * scaleFactor

            -- Store values in a structured table
            table.insert(arr, { x, y, z })

            -- Check for loop termination condition
            if index > 10 and math.abs(x - arr[1][1]) < th and math.abs(y - arr[1][2]) < th and math.abs(z - arr[1][3]) < th then
                break
            end
        end

        return arr
    end

    result = calc()

    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
