-- Based upon Fusion's built-in "Set Metadata.fuse"

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMetadataFromText"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Meta\\Text",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion image with metadata added from text.",
    REGS_OpIconString = FUSE_NAME,
    REG_NoMotionBlurCtrls = true,
    REG_NoBlendCtrls = true,
    REG_OpNoMask = true,
    REG_NoPreCalcProcess = true, -- make default PreCalcProcess() behaviour be to call Process() rather than automatic pass through.
    REG_SupportsDoD = true,
    REG_Fuse_NoJIT = true,
    REGS_IconID = "Icons.Tools.Icons.StickyNote",
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InFieldName = self:AddInput("Field Name", "FieldName", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 3,
    })

    InFieldValue = self:AddInput("Field Value", "FieldValue", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2, -- Make this input #2 so it is the default input for text connections that are drag/dropped on the node.
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        -- InImage:SetAttrs({LINK_Visible = visible})

        InFieldName:SetAttrs({LINK_Visible = visible})
        InFieldValue:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    local f_name = InFieldName:GetValue(req).Value
    local f_value = InFieldValue:GetValue(req).Value
    local img = InImage:GetValue(req)

    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    img:Crop(result, {})

    if (f_name ~= "") then
        local newmetadata = result.Metadata or {}

        if f_value ~= "" then
            -- create subtables for dotted field names
            local key
            local subtable = newmetadata
            for key in string.gmatch(f_name, "([%w_]+)%.") do
                subtable[key] = subtable[key] or {}
                subtable = subtable[key]
            end

            -- get final key
            f_name = string.match(f_name, "[%w_]+$")

            -- look for tables in f_value
            local tbl = string.match(f_value, "%s*%b{}%s*")
            if tbl then
                -- parse tables
                subtable[f_name] = eyeon.readstring(f_value)
            else
                subtable[f_name] = f_value
            end
        else
            newmetadata[f_name] = nil
        end

        result.Metadata = newmetadata
    end

    OutImage:Set(req, result)
end
