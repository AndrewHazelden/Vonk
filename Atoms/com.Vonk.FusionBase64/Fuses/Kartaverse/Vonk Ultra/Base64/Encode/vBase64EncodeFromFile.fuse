-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vBase64EncodeFromFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Base64\\Encode",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Base64 encodes a file into a Fusion Text object.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("File", "File", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        LINK_Main = 1,
        FCS_FilterString =  "Any Filetype (*.*)|*.*"
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('File'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InFile:SetAttrs({LINK_Visible = true})
        else
            InFile:SetAttrs({LINK_Visible = false})
        end
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local bin_data = base64utils.read_file(abs_path, "rb")
    local base64_str = base64utils.base64encode(bin_data)
    
    local out = Text(base64_str)

    OutText:Set(req, out)
end
