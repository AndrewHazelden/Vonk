Atom {
	Name = "Vonk | FusionArray",
	Category = "Kartaverse/Vonk Ultra/Modifiers",
	Author = "Kristof & Cédric",
	Version = 1.912,
	Date = {2025, 2, 9},
	Description = [[<p>FusionArray is a node based JSON formatted array processing library for Blackmagic Design Fusion.</p>

<p>Open-Source License<br>
The Vonk fuses are licensed under a GPL v3 license.</p>

<p>The original Spicy Acorn Vonk toolset was created by<br>
<a href="mailto:xmnr0x23@gmail.com">Kristof Indeherberge</a><br>
<a href="mailto:duriau.cedric@live.be">Cédric Duriau</a></p>

<p>The Vonk Ultra fork is maintained by:<br>
<a href="mailto:andrew@andrewhazelden.com">Andrew Hazelden</a></p>
]],
	Deploy = {
		"Defaults/Fuse.vArrayDoString_vArrayDoString.setting.bak",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromCSV.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromDataWindow.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromJSON.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromLuaTable.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromMediaIn.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromMetadata.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromXML.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Create/vArrayFromYAML.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Flow/vArraySwitch.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Flow/vArrayWireless.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Key Value/vArrayGet.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Key Value/vArrayGetElement.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Key Value/vArrayGetIndex.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Key Value/vArrayGetKey.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Key Value/vArrayKeys.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Script/vArrayDoString.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Substring/vArraySubReturn.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Temporal/vArrayTimeSpeed.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Temporal/vArrayTimeStretch.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayConcatenate.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayCountElement.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayCountSubElements.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayJoin.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayMatch.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArraySize.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArraySlice.fuse",
		"Fuses/Kartaverse/Vonk Ultra/Array/Utility/vArrayViewer.fuse",
		"Modules/Lua/varrayutils.lua",
	},
	Dependencies = {
			"com.Vonk.FusionJSON",
	},
}
