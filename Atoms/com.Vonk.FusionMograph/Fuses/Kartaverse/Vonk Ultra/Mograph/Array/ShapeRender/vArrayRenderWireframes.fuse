-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayRenderWireframes"
DATATYPE = "Image"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\ShapeRender",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "For testing- Create a polygon wireframe shapes from a ScriptVal based Lua table of XY point pairs, and a ScriptVal Lua table of edge index values.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.sRender",
    REG_Version = 100,
})
function Create()
    -- [[ Creates the user interface. ]]
    InRender = self:AddInput("RENDER", "Render", {
        LINKID_DataType    = "Number",
        MBTNC_ShowName     = false,
        MBTNC_ForceButtons = true,
        INPID_InputControl = "MultiButtonControl",
        INP_Default        = 0,
        { MBTNC_AddButton = "SIMPLE RENDER",  MBTNCD_ButtonWidth = 1 / 2, },
        { MBTNC_AddButton = "COMPLEX RENDER", MBTNCD_ButtonWidth = 1 / 2, },
        --INP_DoNotifyChanged = true,
    })
    InSeparator_1 = self:AddInput("Separator_1", "Separator_1", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })
    InUnConnectLines = self:AddInput("Use Edge Data", "unconnectlines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 1,
        ICD_Width = 1,
    })
    InShowLines = self:AddInput("Show Lines", "showlines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 1,
        ICD_Width = 1,
    })
    InShowDots = self:AddInput("Show Dots", "showdots", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 1,
        ICD_Width = 1,
    })
    InShowIndex = self:AddInput("Show Index", "showindex", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 1,
        ICD_Width = 1,
    })
    InLineWidth = self:AddInput("Line Width", "lineWidth", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 0,
        INP_MaxScale       = 0.1,
        INP_Default        = 0.01,
    })
    InDotWidth = self:AddInput("Dot Width", "DotWidth", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 0,
        INP_MaxScale       = 0.1,
        INP_Default        = 0.0,
    })
    self:BeginControlNest("Line Settings", "LINESETTINGS", false, {})
    InSeparator_2 = self:AddInput("Separator_2", "Separator_2", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })
    InLine_R = self:AddInput("Red", "Red_line", {
        ICS_Name           = "Line Color",
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 0,
        INP_MaxScale       = 1.0,
        CLRC_ShowWheel     = false,
        IC_ControlGroup    = 1,
        IC_ControlID       = 0,
    })
    InLine_G = self:AddInput("Green", "Green_line", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1,
        IC_ControlGroup    = 1,
        IC_ControlID       = 1,
    })
    InLine_B = self:AddInput("Blue", "Blue_line", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1,
        IC_ControlGroup    = 1,
        IC_ControlID       = 2,
    })
    InLine_A = self:AddInput("Alpha", "Alpha_line", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale       = 0.0,
        INP_MaxScale       = 1.0,
        INP_Default        = 1.0,
        IC_ControlGroup    = 1,
        IC_ControlID       = 3,
    })
    self:EndControlNest()

    self:BeginControlNest("Dot Settings", "DOTSETTINGS", false, {})
    InDotSides = self:AddInput("Dot Sides", "dotSides", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer        = true,
        INP_MinScale       = 3,
        INP_MinAllowed     = 3,
        INP_MaxScale       = 10,
        INP_Default        = 6,
    })
    InDotRadius = self:AddInput("Dot Radius", "dotRadius", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MaxScale       = 0.1,
        INP_Default        = 0.03,
    })
    InHideDots = self:AddInput("Hide Default Dots", "hidedots", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 0,
        ICD_Width = 1,
    })
    InHideCustomDots = self:AddInput("Hide Custom Dots", "hidecustomdots", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 0,
        ICD_Width = 1,
    })
    InOutlineDots = self:AddInput("Render Outline Dots", "outlinedots", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_DoNotifyChanged = true,
        INP_Integer = true,
        INP_Default = 0,
        ICD_Width = 1,
    })
    InScaleDots = self:AddInput("Scale Dots", "ScaleDots", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 0,
        INP_MaxScale       = 1,
        INP_Default        = 1,
    })
    InSeparator_3 = self:AddInput("Separator_3", "Separator_3", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })
    InDot_R = self:AddInput("Red", "Red_dot", {
        ICS_Name           = "Dot Color",
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1.0,
        INP_MaxScale       = 1.0,
        CLRC_ShowWheel     = false,
        IC_ControlGroup    = 2,
        IC_ControlID       = 0,
    })
    InDot_G = self:AddInput("Green", "Green_dot", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 0.0,
        IC_ControlGroup    = 2,
        IC_ControlID       = 1,
    })
    InDot_B = self:AddInput("Blue", "Blue_dot", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 0.5,
        IC_ControlGroup    = 2,
        IC_ControlID       = 2,
    })
    InDot_A = self:AddInput("Alpha", "Alpha_dot", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale       = 0.0,
        INP_MaxScale       = 1.0,
        INP_Default        = 1.0,
        IC_ControlGroup    = 2,
        IC_ControlID       = 3,
    })
    self:EndControlNest()

    self:BeginControlNest("Text Settings", "TEXTSETTINGS", false, {})
    InTextSize = self:AddInput("Text Size", "TextSize", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale       = 0.01,
        INP_MinAllowed     = 0,
        INP_MaxScale       = 0.1,
        INP_Default        = 0.025,
    })
    InTextStyle = self:AddInput("Text Style", "TextStyle", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default        = 1.0,
        MBTNC_ShowName     = false,
        {
            MBTNC_AddButton    = "Regular",
            MBTNCD_ButtonWidth = 0.3333,
        },
        {
            MBTNC_AddButton    = "Bold",
            MBTNCD_ButtonWidth = 0.3333,
        },
    })
    InSeparator_4 = self:AddInput("Separator_4", "Separator_4", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })
    InTxt_R = self:AddInput("Red", "Red_txt", {
        ICS_Name           = "Txt Color",
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1.0,
        INP_MaxScale       = 1.0,
        CLRC_ShowWheel     = false,
        IC_ControlGroup    = 3,
        IC_ControlID       = 0,
    })
    InTxt_G = self:AddInput("Green", "Green_txt", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1,
        IC_ControlGroup    = 3,
        IC_ControlID       = 1,
    })
    InTxt_B = self:AddInput("Blue", "Blue_txt", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_Default        = 1,
        IC_ControlGroup    = 3,
        IC_ControlID       = 2,
    })
    InTxt_A = self:AddInput("Alpha", "Alpha_txt", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "ColorControl",
        INP_MinScale       = 0.0,
        INP_MaxScale       = 1.0,
        INP_Default        = 1.0,
        IC_ControlGroup    = 3,
        IC_ControlID       = 3,
    })

    self:EndControlNest()
    InSeparator_5 = self:AddInput("Separator_5", "Separator_5", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })
    InFade = self:AddInput("Fade Color", "Fade", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0.0001,
        INP_MaxAllowed = 1.0,
        INP_Default = 0.1,
    })
    InSeparator_6 = self:AddInput("Separator_6", "Separator_6", {
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
        IC_Visible = false,
    })

    self:BeginControlNest("Style Settings", "stylesettings", false, {})
    InFilter = self:AddInput("Filter", "Filter", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default        = 3.0,
        {
            MBTNC_AddButton    = "Box",
            MBTNCD_ButtonWidth = 0.25,
        },
        {
            MBTNC_AddButton    = "Bartlett",
            MBTNCD_ButtonWidth = 0.25,
        },
        {
            MBTNC_AddButton    = "Multi-box",
            MBTNCD_ButtonWidth = 0.25,
        },
        {
            MBTNC_AddButton    = "Gaussian",
            MBTNCD_ButtonWidth = 0.25,
        },
        IC_Visible = false,
        PC_Visible = false
    })
    InSoftEdge = self:AddInput("Soft Edge", "SoftEdge", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 0,
        INP_MaxScale       = 50,
        INP_Default        = 0,
        IC_Visible         = false,
        PC_Visible         = false
    })
    InLineType = self:AddInput("Line Type", "LineType", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default        = 0.0,
        MBTNC_ShowName     = false,
        {
            MBTNC_AddButton    = "Solid",
            MBTNCD_ButtonWidth = 0.33,
        },
        {
            MBTNC_AddButton    = "Dash",
            MBTNCD_ButtonWidth = 0.34,
        },
        {
            MBTNC_AddButton    = "Dot",
            MBTNCD_ButtonWidth = 0.33,
        },
        {
            MBTNC_AddButton    = "Dash Dot",
            MBTNCD_ButtonWidth = 0.5,
        },
        {
            MBTNC_AddButton    = "Dash Dot Dot",
            MBTNCD_ButtonWidth = 0.5,
        },
    })
    InJoinType = self:AddInput("Join Type", "JoinType", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default        = 2.0,
        MBTNC_ShowName     = false,
        {
            MBTNC_AddButton    = "Bevel",
            MBTNCD_ButtonWidth = 0.3333,
        },
        {
            MBTNC_AddButton    = "Round",
            MBTNCD_ButtonWidth = 0.3333,
        },
    })

    self:EndControlNest()

    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
    InPoint = self:AddInput("Array Point", "Point", {
        LINKID_DataType = "Text",
        INPID_InputControl = "ImageControl",
        LINK_Main = 3,
    })
    InEdge = self:AddInput("Array Edge", "Edge", {
        LINKID_DataType = "Text",
        INPID_InputControl = "ImageControl",
        LINK_Main = 4,
    })
    InShape = self:AddInput("Array Shape", "Shape", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 4,
    })
    Output = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.
        :param inp: Input that triggered a signal.
        :type inp: Input
        :param param: Parameter object holding the (new) value.
        :type param: Parameter
        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowDots then
        if param.Value == 1.0 then
            InDotRadius:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InDotSides:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InOutlineDots:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InHideDots:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InHideCustomDots:SetAttrs({ IC_Visible = true, PC_Visible = true })
            InScaleDots:SetAttrs({ IC_Visible = true, PC_Visible = true })
        else
            InOutlineDots:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InDotRadius:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InDotSides:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InHideDots:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InHideCustomDots:SetAttrs({ IC_Visible = false, PC_Visible = false })
            InScaleDots:SetAttrs({ IC_Visible = false, PC_Visible = false })
        end
    end
end

-- ============================================================================

function createIndex_string(text, size, style)
    local spacer = 13
    local mat = Matrix4()
    local font = "Open Sans"
    local style = style
    --local size = tbl[1]
    local font = TextStyleFont(font, style)
    local tfm = TextStyleFontMetrics(font)
    local line_width = 0
    -- This is the distance between this line and the next one.
    mat:Scale(1.0 / tfm.Scale, 1.0 / tfm.Scale, 1.0)
    mat:Scale(size, size, 1)
    mat:Scale(1.9, 1.9, 1)
    -- set the initial baseline position of the text cursor
    local sh, ch, prevch
    local shape = Shape()
    local shw = 0
    local x_move = 0
    for i = 1, #text do
        ch = text:byte(i)
        local cw = tfm:CharacterWidth(ch) * spacer * size
        mat:Move(cw / 2, 0, 0)
        x_move = x_move + cw + 0.0018
        sh = tfm:GetCharacterShape(ch, false)
        sh = sh:TransformOfShape(mat)
        shape:AddShape(sh)
        shw = shw + cw
        -- Move to the next position for the next character
        mat:Move(cw, 0, 0)
    end

    local text_height = (tfm.TextAscent + tfm.TextDescent + tfm.TextExternalLeading) * size
    mat:Identity()
    -- mat:Move(-x_move / 2, -0.01, 0)
    mat:Move(-x_move / 2, (text_height - size) / 2, 0)
    shape = shape:TransformOfShape(mat)
    return shape
end

-- function createPoly Shapes-------------------------------
function createPoly3(sides, r, angle, tbl_Shape, hideDots, hideCustomDots, index)
    local mat = Matrix4()
    mat:Identity()
    mat:Move(0, 0, 0)
    -- shortcuts for faster access
    local sin = math.sin
    local cos = math.cos
    local sh = Shape()
    --local sh_number = createIndex_string(index,0.025)
    local sh_holder = Shape()
    local x = cos(angle) * r
    local y = sin(angle) * r
    sh:MoveTo(x, y)
    if hideDots < 0.5 then
        for i = 1, sides - 1 do
            x = cos(i / sides * math.pi * 2 + angle) * r
            y = sin(i / sides * math.pi * 2 + angle) * r
            sh:LineTo(x, y)
        end
        sh:Close()
        sh_holder:AddShape(sh)
    end
    if hideCustomDots < 0.5 then
        sh_holder:AddShape(tbl_Shape)
    end
    sh_holder = sh_holder:TransformOfShape(mat)
    return sh_holder
end

function Process(req)
    local img               = InImage:GetValue(req)

    local array_str_point = InPoint:GetValue(req).Value
    local array_str_edge = InEdge:GetValue(req).Value
    local array_str_shape = InShape:GetValue(req).Value

    local tbl = {}
    if array_str_point and array_str_point ~= "" then
        local inp_point_tbl = jsonutils.decode(array_str_point)
        if inp_point_tbl ~= nil and type(inp_point_tbl) == "table" then
            tbl = inp_point_tbl["array"]
        end
    end

    local tbl_Edge = {}
    if array_str_edge and array_str_edge ~= "" then
        local inp_edge_tbl = jsonutils.decode(array_str_edge)
        if inp_edge_tbl ~= nil and type(inp_edge_tbl) == "table" then
            tbl_Edge = inp_edge_tbl["array"]
        end
    end

    local tbl_Shape = {}
    if array_str_shape and array_str_shape ~= "" then
        local inp_shape_tbl = jsonutils.decode(array_str_shape)
        if inp_shape_tbl ~= nil and type(inp_shape_tbl) == "table" then
            tbl_Shape = inp_shape_tbl["array"]
        end
    end

--    dump(tbl)
--    dump(tbl_Edge)
--    dump(tbl_Shape)

    local segmentDot        = Shape()
    local segmentIndex      = Shape()
    local segmentLines      = Shape()

    local out               = img:CopyOf()
    local linetype          = math.floor(InLineType:GetValue(req).Value + 0.5) + 1
    local jointype          = math.floor(InJoinType:GetValue(req).Value + 0.5) + 1
    local width             = InLineWidth:GetValue(req).Value
    local dot_width         = InDotWidth:GetValue(req).Value
    local soft_edge         = InSoftEdge:GetValue(req).Value
    local dot_sides         = InDotSides:GetValue(req).Value
    local dot_radius        = InDotRadius:GetValue(req).Value
    local filter            = math.floor(InFilter:GetValue(req).Value + 0.5) + 1
    local line_r            = InLine_R:GetValue(req).Value
    local line_g            = InLine_G:GetValue(req).Value
    local line_b            = InLine_B:GetValue(req).Value
    local line_a            = InLine_A:GetValue(req).Value
    local dot_r             = InDot_R:GetValue(req).Value
    local dot_g             = InDot_G:GetValue(req).Value
    local dot_b             = InDot_B:GetValue(req).Value
    local dot_a             = InDot_A:GetValue(req).Value
    local txtSize           = InTextSize:GetValue(req).Value
    local txtStyle          = math.floor(InTextStyle:GetValue(req).Value) + 1
    local txt_r             = InTxt_R:GetValue(req).Value
    local txt_g             = InTxt_G:GetValue(req).Value
    local txt_b             = InTxt_B:GetValue(req).Value
    local txt_a             = InTxt_A:GetValue(req).Value
    local fade              = 1.0 - InFade:GetValue(req).Value
    local hideDots          = InHideDots:GetValue(req).Value
    local hideCustomDots    = InHideCustomDots:GetValue(req).Value
    local scaleDots         = InScaleDots:GetValue(req).Value

    local outline_line_type = {
        "OLT_Solid",
        "OLT_Dash",
        "OLT_Dot",
        "OLT_DashDot",
        "OLT_DashDotDot",
    }
    local blurfilters       = {
        "BT_Box",
        "BT_Bartless",
        "BT_MultiBox",
        "BT_Gaussian",
    }
    local outline_join_type = {
        "OJT_Bevel",
        "OJT_Round",
    }
    local outline_cap_type  = {
        "OCT_Butt",
        "OCT_Square",
        "OCT_Round",
    }
    local text_Style        = {
        "Regular",
        "Bold",
    }

    local mat               = Matrix4()
    mat:Identity()
    mat:Move(0, 0, 0)
    local ic = ImageChannel(out, 8)
    local cs = ChannelStyle()
    local fs = FillStyle()
    ic:SetStyleFill(fs)
    -- ================================================================
    -- ================================================================
    -- ----------  Add Lines ------------------------------------------
    local shape = Shape()
    local s_point = {}
    local e_point = {}

    for key, faces in ipairs(tbl_Edge) do
        if type(faces) == "table" then
            if #faces >= 2 then
                for i = 2, #faces do
                    -- print(faces[i-1], "->", faces[i])
                    -- dump(tbl[tonumber(faces[i-1])], tbl[tonumber(faces[i])])
                    table.insert(s_point, tbl[tonumber(faces[i-1])])
                    table.insert(e_point, tbl[tonumber(faces[i])])
                end
                table.insert(s_point, tbl[tonumber(faces[#faces])])
                table.insert(e_point, tbl[tonumber(faces[1])])
            end
        end
    end

    -- dump(s_point)
    -- dump(e_point)

    -- ----------  Add UnConnect Lines --------------------------------
    if InUnConnectLines:GetValue(req).Value > 0.5 then
        for i = 1, #e_point do
            local shape_line = Shape()
            if e_point[i][2] == nil then
            else
                mat:Identity()
                mat:Scale(0, 0, 1)
                mat:Move(s_point[i][1], s_point[i][2], 0)
                shape_line:MoveTo(s_point[i][1], s_point[i][2])
                shape_line = shape_line:TransformOfShape(mat)
                shape_line:LineTo(e_point[i][1], e_point[i][2])
                shape_line = shape_line:OutlineOfShape(width, outline_line_type[linetype], outline_join_type[jointype],
                    "OCT_Square", (req:IsQuick() and 8 or 16))
                -- check which Render is selected:
                if InRender:GetValue(req).Value > 0.5 then
                    -- print( "--- Complex Renderer---" )
                    -- ---------- Add Lines to Image
                    if InShowLines:GetValue(req).Value > 0.5 then
                        -- ----------  Add Lines Color
                        cs.Color = Pixel({
                            R = line_r * fade ^ i,
                            G = line_g * fade ^ i,
                            B = line_b * fade ^ i,
                            A = line_a
                        })
                        -- ----------  Add Lines to Image
                        ic:ShapeFill(shape_line)
                        ic:PutToImage("CM_Merge", cs)
                    end
                else
                    -- Simple
                    segmentLines:AddShape(shape_line)
                end
            end
        end
    else
        for i = 1, #tbl do
            if tbl[i][2] == nil then
            else
                mat:Identity()
                mat:Move(tbl[i][1], tbl[i][2], 0)
                shape:LineTo(tbl[i][1], tbl[i][2])
            end
        end
        shape = shape:OutlineOfShape(width, outline_line_type[linetype], outline_join_type[jointype], "OCT_Square",
            (req:IsQuick() and 8 or 16))
        -- check which Render is selected:
        if InRender:GetValue(req).Value > 0.5 then
            -- print( "--- Complex Renderer---" )
            -- ---------- Add Lines to Image
            if InShowLines:GetValue(req).Value > 0.5 then
                -- ----------  Add Lines Color
                cs.Color = Pixel({
                    R = line_r,
                    G = line_g,
                    B = line_b,
                    A = line_a
                })
                -- ----------  Add Lines to Image
                ic:ShapeFill(shape)
                ic:PutToImage("CM_Merge", cs)
            end
        else
            -- Simple
            segmentLines:AddShape(shape)
        end
    end
    -- ================================================================
    -- ================================================================
    -- ----------  Add Dots -------------------------------------------
    for i = 1, #tbl do
        if tbl[i][2] == nil then
        else
            mat:Identity()
            mat:Scale(scaleDots, scaleDots, 1)
            mat:Move(tbl[i][1], tbl[i][2], 0)
            local shape = Shape()
            local shapeIndex = Shape()
            shape = createPoly3(dot_sides, dot_radius, 0, tbl_Shape[1], hideDots, hideCustomDots, tostring(i))
            shape = shape:TransformOfShape(mat)
            shapeIndex = createIndex_string(tostring(i), txtSize, text_Style[txtStyle])
            shapeIndex = shapeIndex:TransformOfShape(mat)
            -- ----------  Add Outline DOT
            if InOutlineDots:GetValue(req).Value > 0.5 then
                shape = shape:OutlineOfShape(dot_width, outline_line_type[linetype], outline_join_type[jointype],
                    "OCT_Square", (req:IsQuick() and 8 or 16))
                --  shapeIndex = shapeIndex:OutlineOfShape(width, outline_line_type[linetype], outline_join_type[jointype],"OCT_Square", (req:IsQuick() and 8 or 16))
            end
            -- check which Render is selected:
            if InRender:GetValue(req).Value > 0.5 then
                --print( "--- Complex Renderer---" )
                -- ----------  Add DOT to Image
                if InShowDots:GetValue(req).Value > 0.5 then
                    cs.Color = Pixel({
                        R = dot_r * fade ^ i,
                        G = dot_g * fade ^ i,
                        B = dot_b * fade ^ i,
                        A = dot_a,
                    })
                    ic:ShapeFill(shape)
                    ic:PutToImage("CM_Merge", cs)
                end
                if InShowIndex:GetValue(req).Value > 0.5 then
                    cs.Color = Pixel({
                        R = txt_r * fade ^ i,
                        G = txt_g * fade ^ i,
                        B = txt_b * fade ^ i,
                        A = txt_a,
                    })
                    ic:ShapeFill(shapeIndex)
                    ic:PutToImage("CM_Merge", cs)
                end
            else
                -- Simple
                --print( "---select 1 Simple: ",InRender:GetValue(req).Value)
                segmentDot:AddShape(shape)
                segmentIndex:AddShape(shapeIndex)
            end
        end
    end
    -- ================================================================
    -- ================================================================
    -- using simple renderer
    -- check which Render is selected:
    if InRender:GetValue(req).Value > 0.5 then
        -- print( "---select 0 Complex:",InRender:GetValue(req).Value)
    else
        -- Simple
        --print( "---simple renderer---")
        --Add Lines to Image
        if InShowLines:GetValue(req).Value > 0.5 then
            cs.Color = Pixel({
                R = line_r,
                G = line_g,
                B = line_b,
                A = line_a
            })
            ic:ShapeFill(segmentLines)
            ic:PutToImage("CM_Merge", cs)
        end
        --Add DOT to Image
        if InShowDots:GetValue(req).Value > 0.5 then
            cs.Color = Pixel({
                R = dot_r,
                G = dot_g,
                B = dot_b,
                A = dot_a
            })
            ic:ShapeFill(segmentDot)
            ic:PutToImage("CM_Merge", cs)
        end
        --Add DOT to Image
        if InShowIndex:GetValue(req).Value > 0.5 then
            cs.Color = Pixel({
                R = txt_r,
                G = txt_g,
                B = txt_b,
                A = txt_a
            })
            ic:ShapeFill(segmentIndex)
            ic:PutToImage("CM_Merge", cs)
        end
    end
    Output:Set(req, out)
end
