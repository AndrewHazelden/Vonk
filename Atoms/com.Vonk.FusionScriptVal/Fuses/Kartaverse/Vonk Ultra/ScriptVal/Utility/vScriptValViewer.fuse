-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValViewer"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View the Fusion ScriptVal object contents in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.StickyNote",
})

function Create()
    -- [[ Creates the user interface. ]]

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })
    
    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ScriptValListControl",
        IC_NoLabel = true,
        LC_Rows = 24,
        INP_Passive = true,
        LINK_Main = 1
    })

    -- The output node connection where data is pushed out of the fuse
    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InDisplayLines then
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InScriptVal:SetAttrs({LC_Rows = lines})
        -- Toggle the visibility to refresh the inspector view
        InScriptVal:SetAttrs({IC_Visible = false})
        InScriptVal:SetAttrs({IC_Visible = true})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local show_dump = InShowDump:GetValue(req).Value

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        local txt_str = bmd.writestring(tbl)
        dump(txt_str)
    end

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
