-- vJSONTransform renamed to vJSONCameraProjection
-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValCameraProjection"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\ScriptVal\\Modify",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Transforms ScriptVals using a perspective projection matrix.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.Camera3D",
})

function Create()
    -- [[ Creates the user interface. ]]
    local ConnectionLabel = [[
<table align="center" cellspacing="8">
  <tr>
  <td style="text-align: center; padding: 5px;">[vScriptValFromOBJ] →</td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[vScriptValCameraProjection]</td>
    <td style="text-align: center; padding: 5px;">→ [vScriptValRenderWireframes]</td>
  </tr>
</table>
]]

    InConnectionLabel = self:AddInput(ConnectionLabel, "ConnectionLabel", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_ControlPage = -1,
        IC_NoLabel = true,
        IC_NoReset = true,
    })
    self:BeginControlNest("Translation", "Translation", true, { LBLC_PickButton = false })

    TranslateX = self:AddInput("X Offset", "Translate.X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
    })

    TranslateY = self:AddInput("Y Offset", "Translate.Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
    })

    TranslateZ = self:AddInput("Z Offset", "Translate.Z", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1.0,
    })
    self:EndControlNest()

    self:BeginControlNest("Rotation", "Rotation", true, { LBLC_PickButton = false })
    InRotOrder = self:AddInput("Rotation Order", "RotOrder", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0,
        { MBTNC_AddButton = " XYZ ", },
        { MBTNC_AddButton = " XZY ", },
        { MBTNC_AddButton = " YXZ ", },
        { MBTNC_AddButton = " YZX ", },
        { MBTNC_AddButton = " ZXY ", },
        { MBTNC_AddButton = " ZYX ", },
        IC_Visible = false,
        PC_Visible = false
    })

    RotateX = self:AddInput("X Rotation", "Rotate.X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_MaxScale = 360.0,
        IC_Steps = 1,
    })

    RotateY = self:AddInput("Y Rotation", "Rotate.Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_MaxScale = 360.0,
        IC_Steps = 1,
    })

    RotateZ = self:AddInput("Z Rotation", "Rotate.Z", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_MaxScale = 360.0,
    })

    self:EndControlNest()
    self:BeginControlNest("Scale", "Scale", true, { LBLC_PickButton = false })
    InLockScale = self:AddInput("Lock X/Y", "LockXY", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1,
        INP_DoNotifyChanged = true,
    })

    ScaleX = self:AddInput("X Scale", "Scale.X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1.0,
    })

    ScaleY = self:AddInput("Y Scale", "Scale.Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1.0,
        IC_Visible = false,
        PC_Visible = false
    })
    self:EndControlNest()



    self:BeginControlNest("perspective projection", "perspective_projection", true, {
        LBLC_PickButton = false,
    })
    InFov = self:AddInput("Field of view", "fov", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Integer = false,
        INP_Default = 90.0,
    })
    InAspect_ratio = self:AddInput("Screen aspect ratio", "aspect_ratio", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 16 / 9,
        IC_Visible = false,
        PC_Visible = false
    })

    InNear = self:AddInput("Near clipping plane", "near", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 0.1,
        IC_Visible = false,
        PC_Visible = false
    })
    InFar = self:AddInput("Far clipping plane", "for", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 1000,
        IC_Visible = false,
        PC_Visible = false
    })


    self:EndControlNest()

    self:BeginControlNest("Shear", "Shear", true, {
        LBLC_PickButton = false,
        IC_Visible = false,
        PC_Visible = false
    })
    ShearX = self:AddInput("X Shear", "Shear.X", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 0.0,
        IC_Visible = false,
        PC_Visible = false
    })

    ShearY = self:AddInput("Y Shear", "Shear.Y", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = false,
        INP_Default = 0.0,
        IC_Visible = false,
        PC_Visible = false

    })
    self:EndControlNest()

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    if inp == InLockScale then
        if param.Value > 0.5 then
            ScaleX:SetAttrs({ LINKS_Name = "Scale" })
            ScaleY:SetAttrs({ IC_Visible = false, PC_Visible = false })
        else
            ScaleX:SetAttrs({ LINKS_Name = "X Scale" })
            ScaleY:SetAttrs({ IC_Visible = true, PC_Visible = true })
        end
    end
end

function convertY(y, ref_img)
    return y * (ref_img.Height * ref_img.YScale) / (ref_img.Width * ref_img.XScale)
end

function Process(req)
    -- [[ Creates the output. ]]
    local arrayA = InScriptVal:GetValue(req):GetValue() or {}

    local _translateX = TranslateX:GetValue(req).Value
    local _translateY = TranslateY:GetValue(req).Value
    local _translateZ = TranslateZ:GetValue(req).Value

    local _rotationZ = RotateZ:GetValue(req).Value --* (math.pi * 200) / 360.0
    local _rotationX = RotateX:GetValue(req).Value --* (math.pi * 200) / 360.0
    local _rotationY = RotateY:GetValue(req).Value --* (math.pi * 200) / 360.0

    local _scaleX = ScaleX:GetValue(req).Value
    local _scaleY = _scaleX

    local _shearX = ShearX:GetValue(req).Value
    local _shearY = ShearY:GetValue(req).Value

    if InLockScale:GetValue(req).Value < 0.5 then
        _scaleX = ScaleX:GetValue(req).Value
        _scaleY = ScaleY:GetValue(req).Value
    end


    local fov = InFov:GetValue(req).Value
    local aspect_ratio = InAspect_ratio:GetValue(req).Value
    local near = InNear:GetValue(req).Value
    local far = InFar:GetValue(req).Value


    local result = {}


    -- Define functions to create transformation matrices
    function translation_matrix(tx, ty, tz)
        return {
            { 1, 0, 0, tx },
            { 0, 1, 0, ty },
            { 0, 0, 1, tz },
            { 0, 0, 0, 1 }
        }
    end

    function scale_matrix(sx, sy, sz)
        return {
            { sx, 0,  0,  0 },
            { 0,  sy, 0,  0 },
            { 0,  0,  sz, 0 },
            { 0,  0,  0,  1 }
        }
    end

    -- =========================Rotation Setup =================================

    function rotation_matrix_XYZ(angles)
        local cos_x, sin_x = math.cos(angles[1]), math.sin(angles[1])
        local cos_y, sin_y = math.cos(angles[2]), math.sin(angles[2])
        local cos_z, sin_z = math.cos(angles[3]), math.sin(angles[3])

        -- Rotation matrix around X-axis
        local rotation_x = {
            { 1, 0,     0,      0 },
            { 0, cos_x, -sin_x, 0 },
            { 0, sin_x, cos_x,  0 },
            { 0, 0,     0,      1 }
        }

        -- Rotation matrix around Y-axis
        local rotation_y = {
            { cos_y,  0, sin_y, 0 },
            { 0,      1, 0,     0 },
            { -sin_y, 0, cos_y, 0 },
            { 0,      0, 0,     1 }
        }

        -- Rotation matrix around Z-axis
        local rotation_z = {
            { cos_z, -sin_z, 0, 0 },
            { sin_z, cos_z,  0, 0 },
            { 0,     0,      1, 0 },
            { 0,     0,      0, 1 }
        }

        -- Function to multiply two 4x4 matrices
        local function matrix_multiply_rot(a, b)
            local result = {}
            for i = 1, 4 do
                result[i] = {}
                for j = 1, 4 do
                    result[i][j] = 0
                    for k = 1, 4 do
                        result[i][j] = result[i][j] + a[i][k] * b[k][j]
                    end
                end
            end
            return result
        end

        -- Combine the rotation matrices
        local rotation_xy = matrix_multiply_rot(rotation_x, rotation_y)
        local rotation_xyz = matrix_multiply_rot(rotation_xy, rotation_z)

        return rotation_xyz
    end

    -- Function to perform matrix multiplication
    function matrix_multiply(m1, m2)
        -- Initialize result table with sub-tables
        local result = {}
        for i = 1, #m1 do
            result[i] = {}
        end

        -- Perform matrix multiplication
        for i = 1, #m1 do
            for j = 1, #m2[1] do
                local sum = 0
                for k = 1, #m1[i] do
                    sum = sum + m1[i][k] * m2[k][j]
                end
                result[i][j] = sum
            end
        end
        return result
    end

    -- ================DEMO--DEMO-----------------------------------

    -- Function to convert 3D world coordinates to 2D screen coordinates
    function project(x, y, z, fov, aspect_ratio, near, far)
        -- Perspective projection matrix components
        local scale = 1 / math.tan(fov * 0.5 * math.pi / 180)

        -- Projection matrix components
        local a = aspect_ratio * scale
        local b = scale
        local c = far / (far - near)
        local d = (-far * near) / (far - near)

        -- Apply perspective projection
        local screen_x = a * x / -z
        local screen_y = b * y / -z

        return screen_x, screen_y
    end

    -- ================================================


    -- Function to perform matrix transformation and project to 2D screen coordinates with normalization
    function transform_and_project(array, translateX, translateY, translateZ, scaleX, scaleY, fov, aspect_ratio, near,
                                   far)
        local result = {}

        -- Helper function for perspective projection
        local function project(x, y, z)
            local scale = 1 / math.tan(fov * 0.5 * math.pi / 180)
            local screen_x = (aspect_ratio * scale) * x / -z
            local screen_y = scale * y / -z
            return screen_x, screen_y
        end

        for i = 1, #array do
            local transformed = {}
            for j = 1, #array[i] do
                local value = array[i][j]

                -- Translate
                if j == 1 then
                    value = value + translateX
                elseif j == 2 then
                    value = value + translateY
                elseif j == 3 then
                    value = value + translateZ
                end

                -- Scale
                if j == 1 then
                    value = value * scaleX
                elseif j == 2 then
                    value = value * scaleY
                else
                    value = value * scaleX -- Assuming scaleX is used for other dimensions
                end

                table.insert(transformed, value)
            end

            -- Apply perspective projection
            local x, y, z = unpack(transformed)
            local screen_x, screen_y = project(x, y, z)

            -- Normalize the coordinates
            -- Assuming the screen coordinates are in the range (-aspect_ratio, aspect_ratio) for x and (-1, 1) for y
            -- print( screen_y * (1080 * 1) / (1920 * 1),"<---------------------->",(screen_y / 2) + 0.25)

            local normalized_x = (screen_x / (aspect_ratio * 2)) + 0.5
            local normalized_y = (screen_y / 2) + 0.25 ---Fix this

            table.insert(result, { normalized_x, normalized_y })  ---Fix this add (_z)
        end

        return result
    end

    -- ================================================================================

    -- Apply rotation to the array
    function rotate_array(array, rotation_matrix)
        local rotated_array = {}
        for i = 1, #array do
            local vector = { array[i][1], array[i][2], array[i][3] }
            local result = matrix_multiply({ vector }, rotation_matrix)
            rotated_array[i] = { result[1][1], result[1][2], result[1][3] }
        end

        return rotated_array
    end

    -- ================================================================================

    -- Rotate around X, Y, Z
    local angles            = { math.rad(_rotationX), math.rad(_rotationY), math.rad(_rotationZ) }
    local rotation          = rotation_matrix_XYZ(angles)

    local rotated_array     = rotate_array(arrayA, rotation)
    local _transform_result = transform_and_project(rotated_array, _translateX, _translateY, _translateZ, _scaleX,
        _scaleY, fov, aspect_ratio, near, far)

    --dump(_transform_result)
    OutScriptVal:Set(req, ScriptValParam(_transform_result))
end
