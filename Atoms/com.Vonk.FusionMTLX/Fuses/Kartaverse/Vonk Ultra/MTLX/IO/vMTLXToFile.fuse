-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMTLXToFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\MTLX\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Writes an XML string to a MaterialX file.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

function Create()
    -- [[ Creates the user interface. ]]
 
    InFile = self:AddInput("MTLX File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = false,
        FCS_FilterString =  "MaterialX (*.mtlx)|*.mtlx",
        LINK_Main = 2
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InFontSize = self:AddInput("Font Size", "FontSize", {
        LINKID_DataType      = "Number",
        INPID_InputControl   = "MultiButtonControl",
        INP_Integer          = true,
        INP_Default          = 1.0,
        MBTNC_StretchToFit = true,
        {MBTNC_AddButton     = "12"},
        {MBTNC_AddButton     = "14"},
        {MBTNC_AddButton     = "16"},
        {MBTNC_AddButton     = "18"},
        {MBTNC_AddButton     = "24"},
        {MBTNC_AddButton     = "36"},
        {MBTNC_AddButton     = "48"},
        {MBTNC_AddButton     = "72"},
        INP_DoNotifyChanged  = true,
        INP_Passive = true,
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InXML = self:AddInput("XML" , "XML" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TEC_Lines = 25,
        TEC_FontSize = 18,
        -- TEC_Wrap = true,
        -- INP_Passive = true,
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutXML = self:AddOutput("OutputXML" , "OutputXML" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
    OutFile = self:AddOutput("OutputFile" , "OutputFile" , {
        LINKID_DataType = "Text",
        LINK_Main = 2
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InXML:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InXML:SetAttrs({IC_Visible = false})
        InXML:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InXML:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InXML:SetAttrs({IC_Visible = false})
        InXML:SetAttrs({IC_Visible = true})
    elseif inp == InFontSize then
        -- Change the TextEditControl font size in points
        local fontSizeTbl = {12, 14, 16, 18, 24, 36, 48, 72}
        local fontSizeSelect = fontSizeTbl[param.Value + 1]
        InXML:SetAttrs({TEC_FontSize = fontSizeSelect})
        -- print("[Font Size Pt]", fontSizeSelect)

        -- Toggle the visibility to refresh the inspector view
        InXML:SetAttrs({IC_Visible = false})
        InXML:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt_str = InXML:GetValue(req).Value

    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    textutils.write_file(abs_path, txt_str)
    local out = Text(txt_str)

    OutXML:Set(req, out)
    OutFile:Set(req, Text(abs_path))
end
