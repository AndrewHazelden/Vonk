-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayPointsConnect"
DATATYPE = "Text"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\Array\\Modify",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Connect points in an array",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})
local ConnectionLabel = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center;">[ vArrayUnPacker ]  → </td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;">[ ]] ..
    FUSE_NAME .. [[ ]</td>
    <td style="text-align: center;">  → [ vArray Node ]</td>
  </tr>
</table>
]]
function Create()
    -- [[ Creates the user interface. ]]
    InLabelConnect = self:AddInput(ConnectionLabel, 'LabelConnect', {
        LINKID_DataType = 'Text',
        INPID_InputControl = 'LabelControl',
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_ControlPage = -1,
        IC_NoLabel = true,
        IC_NoReset = true,
    })

    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = 64,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })

    InSelectInput = self:AddInput("Pack To:", "InputNumber", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0.0,
        { MBTNC_AddButton = "X ",    MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "X Y",   MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "X Y Z", MBTNCD_ButtonWidth = 1 / 3, },

        INP_Integer = true,
        INP_DoNotifyChanged = true,
    })

    InGroupSize = self:AddInput("Custom Group Size", "GroupSize", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 2,
        INP_MinScale = 0,
        INP_MaxScale = 10,
        INP_Default = 3,
        IC_Steps = 201,
        INP_MinAllowed = 0,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    Inline4Separator = self:AddInput("line4Separator", "line4Separator", {
        INPID_InputControl = "SeparatorControl",
        IC_Visible = true,
        INP_External = false,
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
        IC_Visible = false
    })
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true,
        IC_Visible = false
    })

    InArray1 = self:AddInput("Array A", "ArrayA", {
        INPID_InputControl = "ImageControl",
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 1,
    })

    InArray2 = self:AddInput("Array B", "ArrayB", {
        INPID_InputControl = "ImageControl",
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 3,
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    if inp == InSelectInput then
        if (param.Value < 0.5) then
            InGroupSize:SetSource(Number(2), 0, 0)
        else
            InGroupSize:SetSource(Number(3), 0, 0)
        end
    end
end

function Process(req)
    local arrayA = jsonutils.decode(InArray1:GetValue(req).Value).array or {}
    local arrayB = jsonutils.decode(InArray2:GetValue(req).Value).array or {}

    local groupSize_val = InGroupSize:GetValue(req).Value

    -- Define the output port
    local result = {}

    -- Define the input arrays and chunk size
    local array1 = arrayA
    local array2 = arrayB

    -- Define the update function
    local function update(groupSize)
        local newArr = {}
        local arr1 = array1
        local arr2 = array2

        -- Combine the input arrays
        if arr1 and not arr2 then
            newArr = arr1
        elseif not arr1 and arr2 then
            newArr = arr2
        elseif arr1 and arr2 then
            for i = 1, math.max(#arr1, #arr2) do
                if arr1[i] then
                    table.insert(newArr, arr1[i])
                end
                if arr2[i] then
                    table.insert(newArr, arr2[i])
                end
            end
        end

        -- Pack newArr into custom groups
        local groupedArr = {}
        for i = 1, #newArr, groupSize do
            local group = {}
            for j = i, math.min(i + groupSize - 1, #newArr) do
                table.insert(group, newArr[j])
            end
            table.insert(groupedArr, group)
        end

        return groupedArr
    end

    -- Define the group size
    local groupSize = groupSize_val

    -- Call the update function with the specified group size
    result = update(groupSize)
    local out_Array = {}
    out_Array["array"] = result
    out_Array["size"] = arrayutils.Length(result)

    local json_str_out = jsonutils.encode(out_Array)
    OutData:Set(req, Text(json_str_out))
end
