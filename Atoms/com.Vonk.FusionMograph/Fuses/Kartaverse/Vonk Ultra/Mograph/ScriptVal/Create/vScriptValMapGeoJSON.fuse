-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValMapGeoJSON"
DATATYPE = "ScriptVal"
-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\ScriptVal\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Map from geographic coordinates.",
    REGS_OpIconString = FUSE_NAME,
    REGS_IconID = "Icons.Tools.Icons.TextPlus",
})

local Label = [[
<table align="center" cellspacing="8">
  <tr>
    <td style="text-align: center;"></td>
    <td style="text-align: center; background-color: #051626; color: white; padding: 5px; font-weight: bold;"> → GeoData example:→   https://cartographyvectors.com/map/64-berlin  </td>
    <td style="text-align: center;"></td>
  </tr>
</table>
]]
function Create()
    -- [[ Creates the user interface. ]]


    InMapZoomWidth = self:AddInput("Map Zoom Width", "mapzoomWidth", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 1000,
        INP_Default = 200,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,

    })
    InCenterLat = self:AddInput("Center Lat", "centerLat", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MaxAllowed = 1e+38,

    })
    InCenterLon = self:AddInput("Center Lon", "centerLon", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 1,
        INP_MaxAllowed = 1e+38,

    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InLabel1 = self:AddInput(Label, 'Label1', {
        LINKID_DataType = 'Text',
        INPID_InputControl = 'LabelControl',
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_NoLabel = true,
        IC_NoReset = true,
    })
    InArray = self:AddInput("Array A", "ArrayA", {
        LINKID_DataType = "Text",
        INP_Required = false,
        LINK_Main = 1,
    })

    OutScriptValData = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
end

function Process(req)
    -- [[ Creates the output. ]]

    local json_str    = InArray:GetValue(req).Value
    local inArr       = jsonutils.decode(json_str)

    local inMapZoom   = InMapZoomWidth:GetValue(req).Value
    local inCenterLat = InCenterLat:GetValue(req).Value
    local inCenterLon = InCenterLon:GetValue(req).Value

    local result      = {}

    -- Function to extract coordinates from GeoJSON
    local function extract_coordinates(geojson)
        local coordinates = {}

        for _, feature in ipairs(geojson.features) do
            local geometry = feature.geometry
            if geometry and geometry.type == "Polygon" then
                for _, polygon in ipairs(geometry.coordinates) do
                    for _, coord in ipairs(polygon) do
                        table.insert(coordinates, { coord[2], coord[1] }) -- GeoJSON uses [lon, lat]
                    end
                end
            elseif geometry and geometry.type == "MultiPolygon" then
                for _, multi_polygon in ipairs(geometry.coordinates) do
                    for _, polygon in ipairs(multi_polygon) do
                        for _, coord in ipairs(polygon) do
                            table.insert(coordinates, { coord[2], coord[1] })
                        end
                    end
                end
            end
        end

        return coordinates
    end


    -- Function to flatten a nested array
    local function flatten_array(input)
        local result = {}

        local function flatten(sub_array)
            for _, value in ipairs(sub_array) do
                if type(value) == "table" then
                    flatten(value)              -- Recursively flatten sub-tables
                else
                    table.insert(result, value) -- Insert non-table values into result
                end
            end
        end

        flatten(input)
        return result
    end

    -- Load and parse the GeoJSON file
    -- https://cartographyvectors.com/map/64-berlin
    local geojson_data = inArr

    -- Extract coordinates
    local map_coordinates = extract_coordinates(geojson_data)


    local function calcLon(lon, mapWidth)
        local x = (lon + 180) * (mapWidth / 360)
        return x
    end

    local function calcLat(lat, mapWidth, mapHeight)
        local latRad = lat * math.pi / 180

        -- get y value
        local mercN = math.log(math.tan((math.pi / 4) + (latRad / 2)))
        local y = (mapHeight / 2) - (mapWidth * mercN / (2 * math.pi))
        return y
    end

    -- Mercator map and center an array of latitudes and longitudes to a local coordinate system

    local mapWidth = 1 - inMapZoom -- 1.289672544080605;
    local mapHeight = 1            -- inMapHeight -- 1;

    local centerLon = calcLon(inCenterLon, mapWidth, mapHeight)
    local centerLat = calcLat(inCenterLat, mapWidth, mapHeight)

    local arr = flatten_array(map_coordinates)

    if not arr then
        outArr:set(nil)
        return
    end

    local newArray = {}

    for i = 1, #arr, 2 do
        local latitude = arr[i]
        local longitude = arr[i + 1]

        local lon = calcLon(longitude, mapWidth, mapHeight)
        local lat = calcLat(latitude, mapWidth, mapHeight)

        lon = lon - centerLon
        lat = lat - centerLat

        table.insert(newArray, { lon, -lat, 0 })
    end

    result = newArray

    OutScriptValData:Set(req, ScriptValParam(result))
end
