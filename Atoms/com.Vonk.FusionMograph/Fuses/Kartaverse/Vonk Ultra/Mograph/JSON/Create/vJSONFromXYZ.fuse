-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vJSONFromXYZ"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Mograph\\JSON\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Convert ASCII XYZ point cloud data into a JSON format",
    REGS_OpIconString = FUSE_NAME,
    REG_OpNoMask = true,
    REG_NoBlendCtrls = true,
    REG_NoObjMatCtrls = true,
    REG_NoMotionBlurCtrls = true,
    REG_NoPreCalcProcess = true,
    -- Should the current time setting be cached?
    -- REG_TimeVariant = true,
    -- REG_Unpredictable = true,
    REGS_IconID = "Icons.Tools.Icons.Shape3D",
})

function Create()
    InInputSource = self:AddInput("Input Source", "InputSource", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        INP_Default = 0,
        { MBTNC_AddButton = "File", MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "URL",  MBTNCD_ButtonWidth = 1 / 3, },
        { MBTNC_AddButton = "Text",  MBTNCD_ButtonWidth = 1 / 3, },
        INP_Integer = true,
        INP_DoNotifyChanged = true,
    })

    InFile = self:AddInput("Filename" , "Filename" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        LINK_Visible = true,
        FCS_FilterString =  "ASCII XYZ (*.xyz)|*.xyz|ASCII XYZ (*.txt)|*.txt|Files (*.*)|*.*|",
        LINK_Main = 1
    })

    InURL = self:AddInput("URL" , "URL" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TECS_Language = "lua",
        TEC_Lines = 10,
        LINK_Main = 3
    })

    InSeparator = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InDensity = self:AddInput("Point Cloud Density", "Density", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        -- TEC_Lines = 1,
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_Default = 100,
        IC_Steps = 11,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 100,
        INP_Integer = true,
    })

    InScale = self:AddInput("Scale File Units By", "Scale", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        -- TEC_Lines = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_Default = 1,
        IC_Steps = 201,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
    })

    OutPoints = self:AddOutput("Points", "Points", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    if inp == InInputSource then
        if param.Value == 0.0 then 
            InFile:SetAttrs({IC_Visible = true, LINK_Visible = true})
            InURL:SetAttrs({IC_Visible = false, LINK_Visible = false})
            InData:SetAttrs({IC_Visible = false, LINK_Visible = false})
        elseif param.Value == 1.0 then
            InFile:SetAttrs({IC_Visible = false, LINK_Visible = false})
            InURL:SetAttrs({IC_Visible = true, LINK_Visible = true})
            InData:SetAttrs({IC_Visible = false, LINK_Visible = false})
        elseif param.Value == 2.0 then
            InFile:SetAttrs({IC_Visible = false, LINK_Visible = false})
            InURL:SetAttrs({IC_Visible = false, LINK_Visible = false})
            InData:SetAttrs({IC_Visible = true, LINK_Visible = true})
        end
    end
end

function Process(req) 
    local pointsTbl = {}
    local edgeTbl = {}

    local density = InDensity:GetValue(req).Value

    local scale = InScale:GetValue(req).Value or 1

    local source = InInputSource:GetValue(req).Value
    if source == 0 then
        -- File
        local rel_path = InFile:GetValue(req).Value
        local abs_path = self.Comp:MapPath(rel_path)
    
        local lineCounter = 0
        for oneLine in io.lines(abs_path) do
            -- One line of data
            -- print('[' .. lineCounter .. '] ' .. oneLine)
    
            -- Modulus gives us a value from 0 to 99
            local skip_rate = lineCounter % 100
            -- Always show the first 100 points before decimating the model
            if lineCounter < 100 or skip_rate >= (100-density) then
                -- Extract the XYZ positions, using %s as a white space character
                local x, y, z = string.match(oneLine, '(%g+)%s(%g+)%s(%g+)')
                table.insert(pointsTbl, {tonumber(x) * scale, tonumber(y) * scale, tonumber(z) * scale})
            end
            lineCounter = lineCounter + 1
        end
    elseif source == 1 then
        -- URL
        local url = InURL:GetValue(req).Value
        local txt_str = textutils.read_url(url)

        local lineCounter = 0
        for oneLine in string.gmatch(txt_str, "[^\r\n]+") do
            -- One line of data
            -- print('[' .. lineCounter .. '] ' .. oneLine)

            -- Modulus gives us a value from 0 to 99
            local skip_rate = lineCounter % 100
            -- Always show the first 100 points before decimating the model
            if lineCounter < 100 or skip_rate >= (100-density) then
                -- Extract the XYZ positions, using %s as a white space character
                local x, y, z = string.match(oneLine, '(%g+)%s(%g+)%s(%g+)')
                table.insert(pointsTbl, {tonumber(x) * scale, tonumber(y) * scale, tonumber(z) * scale})
            end
            lineCounter = lineCounter + 1
        end
    elseif source == 2 then
        -- Text
        local txt_str = InData:GetValue(req).Value

        local lineCounter = 0
        for oneLine in string.gmatch(txt_str, "[^\r\n]+") do
            -- One line of data
            -- print('[' .. lineCounter .. '] ' .. oneLine)

            -- Modulus gives us a value from 0 to 99
            local skip_rate = lineCounter % 100
            -- Always show the first 100 points before decimating the model
            if lineCounter < 100 or skip_rate >= (100-density) then
                -- Extract the XYZ positions, using %s as a white space character
                local x, y, z = string.match(oneLine, '(%g+)%s(%g+)%s(%g+)')
                table.insert(pointsTbl, {tonumber(x) * scale, tonumber(y) * scale, tonumber(z) * scale})
            end
            lineCounter = lineCounter + 1
        end
    end


--    print("[Lua Table]")
--    dump(pointsTbl)

    local point_json_str = jsonutils.encode_indent(pointsTbl, true)
    local point_out = Text(point_json_str)

    OutPoints:Set(req, point_out)
end
